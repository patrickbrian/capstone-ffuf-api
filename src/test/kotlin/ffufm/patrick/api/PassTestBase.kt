package ffufm.patrick.api

import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.security.SpringSecurityAuditorAware
import ffufm.patrick.api.repositories.employee.EmployeeAddressRepository
import ffufm.patrick.api.repositories.employee.EmployeeContactDetailRepository
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.repositories.leaverecord.LeaverecordLeaveRecordRepository
import ffufm.patrick.api.repositories.leaverecord.LeaverecordLeaveTypeRepository
import ffufm.patrick.api.repositories.project.ProjectProjectRepository
import ffufm.patrick.api.repositories.timerecord.TimerecordTimeRecordRepository
import ffufm.patrick.api.repositories.timerecord.TimerecordTimeRecordTypeRepository
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@SpringBootTest(classes = [SBPatrick::class, SpringSecurityAuditorAware::class])
@AutoConfigureMockMvc
abstract class PassTestBase {
    @Autowired
    private lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    private lateinit var employeeAddressRepository: EmployeeAddressRepository

    @Autowired
    private lateinit var employeeContactDetailRepository: EmployeeContactDetailRepository

    @Autowired
    private lateinit var leaveRecordLeaveRecordRepository: LeaverecordLeaveRecordRepository

    @Autowired
    private lateinit var leaveRecordLeaveTypeRepository: LeaverecordLeaveTypeRepository

    @Autowired
    private lateinit var projectProjectRepository: ProjectProjectRepository

    @Autowired
    private lateinit var timeRecordTimeRecordRepository: TimerecordTimeRecordRepository

    @Autowired
    private lateinit var timeRecordTimeRecordTypeRepository: TimerecordTimeRecordTypeRepository

    @Autowired
    lateinit var context: ApplicationContext

    @Before
    fun initializeContext() {
        SpringContext.context = context
    }

    @After
    fun cleanRepository(){
        timeRecordTimeRecordRepository.deleteAll()
        timeRecordTimeRecordTypeRepository.deleteAll()
        leaveRecordLeaveRecordRepository.deleteAll()
        leaveRecordLeaveTypeRepository.deleteAll()
        projectProjectRepository.deleteAll()
        employeeContactDetailRepository.deleteAll()
        employeeAddressRepository.deleteAll()
        employeeEmployeeRepository.deleteAll()

    }
}
