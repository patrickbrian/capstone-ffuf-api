package ffufm.patrick.api.spec.handler.leaverecord.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.repositories.leaverecord.LeaverecordLeaveRecordRepository
import ffufm.patrick.api.repositories.leaverecord.LeaverecordLeaveTypeRepository
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class LeaverecordLeaveRecordHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var leaverecordLeaveRecordRepository: LeaverecordLeaveRecordRepository

    @Autowired
    lateinit var leaverecordLeaveTypeRepository: LeaverecordLeaveTypeRepository

    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        leaverecordLeaveRecordRepository.deleteAll()
        leaverecordLeaveTypeRepository.deleteAll()
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    @WithMockUser
    fun `create should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        ).toDto()
                mockMvc.post("/leaverecords/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(leaverecord)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `getAll should return multiple leave record`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        )

        leaverecordLeaveRecordRepository.save(leaverecord)
        leaverecordLeaveRecordRepository.save(LeaverecordLeaveRecord(
            date = "2022-02-02",
            isApproved = false,
            employee = createdEmployee,
            leaverecord = createdLeaveType
        ))
                mockMvc.get("/leaverecords/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    jsonPath("$.length()") { value(2) }
                    
                }
    }

    @Test
    @WithMockUser
    fun `getByEmployee should return employee's leave records`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        )

        leaverecordLeaveRecordRepository.save(leaverecord)
        leaverecordLeaveRecordRepository.save(LeaverecordLeaveRecord(
            date = "2022-02-02",
            isApproved = false,
            employee = createdEmployee,
            leaverecord = createdLeaveType
        ))

                mockMvc.get("/employee/{id}/leaverecords/", createdEmployee.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    jsonPath("$.length()") { value(2) }
                }
    }

    @Test
    @WithMockUser
    fun `getById should return a leave record`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        )

        val createdLeaveRecord = leaverecordLeaveRecordRepository.save(leaverecord)
                mockMvc.get("/leaverecords/{id}/", createdLeaveRecord.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `remove should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        )

        val createdLeaveRecord = leaverecordLeaveRecordRepository.save(leaverecord)
                mockMvc.delete("/leaverecords/{id}/", createdLeaveRecord.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `update should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        )

        val createdLeaveRecord = leaverecordLeaveRecordRepository.save(leaverecord)

        val body = createdLeaveRecord.copy(
            date = "2022-02-02",
        )
                mockMvc.put("/leaverecords/{id}/", createdLeaveRecord.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }
}
