package ffufm.patrick.api.spec.handler.employee.implementation

import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.employee.EmployeeContactDetailRepository
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.spec.dbo.employee.EmployeeContactDetail
import ffufm.patrick.api.spec.handler.employee.EmployeeContactDetailDatabaseHandler
import ffufm.patrick.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class EmployeeContactDetailDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var employeeContactDetailRepository: EmployeeContactDetailRepository

    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    lateinit var employeeContactDetailDatabaseHandler: EmployeeContactDetailDatabaseHandler


    @Before
    @After
    fun cleanRepositories() {

        employeeContactDetailRepository.deleteAll()
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    fun `create should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val contactdetail = EntityGenerator.contactdetail.copy(
            employee = createdEmployee
        )
        val createdContactdetail = employeeContactDetailRepository.save(contactdetail)

        assertEquals(contactdetail.contactType, createdContactdetail.contactType)
        assertEquals(contactdetail.contactDetails, createdContactdetail.contactDetails)
    }

    @Test
    fun `getById should return a contact detail`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val contactdetail = EntityGenerator.contactdetail.copy(
            employee = createdEmployee
        )
        val createdContactdetail = employeeContactDetailRepository.save(contactdetail)

        val retrievedContactDetail = employeeContactDetailDatabaseHandler
            .getById(requireNotNull(createdContactdetail.id))

        requireNotNull(retrievedContactDetail)

        assertEquals(createdContactdetail.contactType, retrievedContactDetail.contactType)
        assertEquals(createdContactdetail.contactDetails, retrievedContactDetail.contactDetails)
    }

    @Test
    fun `remove should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val contactdetail = EntityGenerator.contactdetail.copy(
            employee = createdEmployee
        )
        val createdContactdetail = employeeContactDetailRepository.save(contactdetail)

        employeeContactDetailDatabaseHandler.remove(requireNotNull(createdContactdetail.id))

        assertEquals(0, employeeContactDetailRepository.findAll().size)

    }

    @Test
    fun `update should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val contactdetail = EntityGenerator.contactdetail.copy(
            employee = createdEmployee
        )
        val createdContactdetail = employeeContactDetailRepository.save(contactdetail)

        val body = createdContactdetail.copy(
            contactDetails = "09111111111"
        ).toDto()

        val updatedContactDetail = employeeContactDetailDatabaseHandler
            .update(body, requireNotNull(createdContactdetail.id))

        assertEquals(createdContactdetail.contactType, updatedContactDetail.contactType)
        assertEquals(createdContactdetail.contactDetails, updatedContactDetail.contactDetails)

    }
}
