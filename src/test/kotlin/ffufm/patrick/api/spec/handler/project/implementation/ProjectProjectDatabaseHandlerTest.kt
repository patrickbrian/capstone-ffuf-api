package ffufm.patrick.api.spec.handler.project.implementation

import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.repositories.project.ProjectProjectRepository
import ffufm.patrick.api.spec.dbo.project.ProjectProject
import ffufm.patrick.api.spec.handler.project.ProjectProjectDatabaseHandler
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class ProjectProjectDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var projectProjectRepository: ProjectProjectRepository

    @Autowired
    lateinit var projectProjectDatabaseHandler: ProjectProjectDatabaseHandler

    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Before
    @After
    fun cleanRepositories() {
        projectProjectRepository.deleteAll()
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    fun `create should work`() = runBlocking {

        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy( employee = createdEmployee )
        val createdProject = projectProjectDatabaseHandler.create(project.toDto())

        assertEquals(project.name, createdProject.name)
        assertEquals(project.description, createdProject.description)

    }

    @Test
    fun `getByEmployee should return employee's project`() = runBlocking {

        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy( employee = createdEmployee )
        projectProjectRepository.saveAll(
            listOf(
                project,
                ProjectProject(
                    name = "Project 2",
                    description = "Description of Project 2",
                    employee = createdEmployee
                )
            )
        )

        val maxResults: Int = 100
        val page: Int = 0

        val projects = projectProjectDatabaseHandler
            .getByEmployee(requireNotNull(createdEmployee.id), maxResults, page)

        assertEquals(2, projects.size)
    }

    @Test
    fun `getById should return a project`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy( employee = createdEmployee )
        val createdProject = projectProjectRepository.save(project)

        val retrievedProject = projectProjectDatabaseHandler.getById(requireNotNull(createdProject.id))

        requireNotNull(retrievedProject)

        assertEquals(createdProject.name, retrievedProject.name)
        assertEquals(createdProject.description, retrievedProject.description)
    }

    @Test
    fun `remove should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy( employee = createdEmployee )
        val createdProject = projectProjectRepository.save(project)

        projectProjectDatabaseHandler.remove(requireNotNull(createdProject.id))
        assertEquals(0, projectProjectRepository.findAll().size)
    }

    @Test
    fun `update should word`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy( employee = createdEmployee )
        val createdProject = projectProjectRepository.save(project)

        val body = createdProject.copy(
            name = "Updated Project Name",
            description = "Updated Project Description"
        ).toDto()

        val updatedProject = projectProjectDatabaseHandler
            .update(body, requireNotNull(createdProject.id))

        assertEquals(body.name, updatedProject.name)
        assertEquals(body.description, updatedProject.description)

    }
}
