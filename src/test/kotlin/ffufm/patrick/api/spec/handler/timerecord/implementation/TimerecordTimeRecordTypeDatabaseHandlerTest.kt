package ffufm.patrick.api.spec.handler.timerecord.implementation

import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.timerecord.TimerecordTimeRecordTypeRepository
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordType
import ffufm.patrick.api.spec.handler.timerecord.TimerecordTimeRecordTypeDatabaseHandler
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class TimerecordTimeRecordTypeDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var timerecordTimeRecordTypeRepository: TimerecordTimeRecordTypeRepository

    @Autowired
    lateinit var timerecordTimeRecordTypeDatabaseHandler: TimerecordTimeRecordTypeDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        timerecordTimeRecordTypeRepository.deleteAll()
    }

    @Test
    fun `create should work`() = runBlocking {
        val timeRecordType = EntityGenerator.timeRecordType

        val createdTimeRecordType = timerecordTimeRecordTypeDatabaseHandler.create(timeRecordType.toDto())

        assertEquals(timeRecordType.shortName, createdTimeRecordType.shortName)
        assertEquals(timeRecordType.longName, createdTimeRecordType.longName)
        assertEquals(timeRecordType.numberOfHours, createdTimeRecordType.numberOfHours)

    }

    @Test
    fun `getById should return a time record type`() = runBlocking {
        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val retrievedTimeRecordType = timerecordTimeRecordTypeDatabaseHandler
            .getById(requireNotNull(createdTimeRecordType.id))

        requireNotNull(retrievedTimeRecordType)

        assertEquals(createdTimeRecordType.shortName, retrievedTimeRecordType.shortName)
        assertEquals(createdTimeRecordType.longName, retrievedTimeRecordType.longName)
        assertEquals(createdTimeRecordType.numberOfHours, retrievedTimeRecordType.numberOfHours)

    }

    @Test
    fun `remove should work`() = runBlocking {
        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)
        timerecordTimeRecordTypeDatabaseHandler.remove(requireNotNull(createdTimeRecordType.id))

        assertEquals(0, timerecordTimeRecordTypeRepository.findAll().size)
    }

    @Test
    fun `update should work`() = runBlocking {
        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val body = createdTimeRecordType.copy(
            shortName = "WP",
            longName = "Waiting Period",
            numberOfHours = 2
        )
        val updatedTimeRecordType = timerecordTimeRecordTypeDatabaseHandler
            .update(body.toDto(), requireNotNull(createdTimeRecordType.id))

        assertEquals(body.shortName, updatedTimeRecordType.shortName)
        assertEquals(body.longName, updatedTimeRecordType.longName)
        assertEquals(body.numberOfHours, updatedTimeRecordType.numberOfHours)

    }
}
