package ffufm.patrick.api.spec.handler.utils

import ffufm.patrick.api.spec.dbo.employee.EmployeeAddress
import ffufm.patrick.api.spec.dbo.employee.EmployeeContactDetail
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployee
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveType
import ffufm.patrick.api.spec.dbo.project.ProjectProject
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecord
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordType

object EntityGenerator {
    val employee = EmployeeEmployee(
        firstName = "Brandon",
        lastName = "Cruz",
        email = "brandon@email.com",
        employeeType = EmployeeTypeEnum.S.name
    )

    val contactdetail = EmployeeContactDetail(
        contactDetails = "09123456789",
        contactType = "Mobile"
    )

    val address = EmployeeAddress(
        street = "Maceda",
        barangay = "506",
        city = "Manila",
        province = "NCR",
        zipCode = "1008"
    )

    val leaveType = LeaverecordLeaveType(
        shortName = "SL",
        maximumAllowed = 2,
        longName = "Sick Leave"

    )
    val leaverecord = LeaverecordLeaveRecord(
        date = "2022-01-01",
        isApproved = false
    )

    val project = ProjectProject(
        name = "Project 1",
        description = "Description of project 1"
    )

    val timeRecordType = TimerecordTimeRecordType(
        shortName = "WH",
        longName = "Working Hours",
        numberOfHours = 8
    )

    val timeRecord = TimerecordTimeRecord(
        date = "2022-01-01",
        category = "Category 1",
        timeIn = "08:00:00",
        timeOut = "18:00:00",
        isUpdated = false,
        remarks = "Sample remarks"
    )
}