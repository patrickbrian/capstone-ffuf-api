package ffufm.patrick.api.spec.handler.employee.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.employee.EmployeeContactDetailRepository
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.spec.dbo.employee.EmployeeContactDetail
import ffufm.patrick.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class EmployeeContactDetailHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var employeeContactDetailRepository: EmployeeContactDetailRepository

    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository


    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        employeeContactDetailRepository.deleteAll()
    }

    @Test
    @WithMockUser
    fun `create should work`() {

        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val contactdetail = EntityGenerator.contactdetail.copy(
            employee = createdEmployee
        )

                mockMvc.post("/contactdetails/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(contactdetail)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `getById should return a contact detail`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val contactdetail = EntityGenerator.contactdetail.copy(
            employee = createdEmployee
        )
        val createdContactDetail = employeeContactDetailRepository.save(contactdetail)

                mockMvc.get("/contactdetails/{id}/", createdContactDetail.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `remove should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val contactdetail = EntityGenerator.contactdetail.copy(
            employee = createdEmployee
        )
        val createdContactDetail = employeeContactDetailRepository.save(contactdetail)
                mockMvc.delete("/contactdetails/{id}/", createdContactDetail.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `update should work`() {

        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val contactdetail = EntityGenerator.contactdetail.copy(
            employee = createdEmployee
        )
        val createdContactDetail = employeeContactDetailRepository.save(contactdetail)
        val body = createdContactDetail.copy(
            contactDetails = "09111111111"
        ).toDto()
                mockMvc.put("/contactdetails/{id}/", createdContactDetail.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }
}
