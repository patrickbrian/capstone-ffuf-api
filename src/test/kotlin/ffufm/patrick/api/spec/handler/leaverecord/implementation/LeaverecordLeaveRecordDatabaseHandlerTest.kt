package ffufm.patrick.api.spec.handler.leaverecord.implementation

import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.repositories.leaverecord.LeaverecordLeaveRecordRepository
import ffufm.patrick.api.repositories.leaverecord.LeaverecordLeaveTypeRepository
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import ffufm.patrick.api.spec.handler.leaverecord.LeaverecordLeaveRecordDatabaseHandler
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class LeaverecordLeaveRecordDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var leaverecordLeaveRecordRepository: LeaverecordLeaveRecordRepository

    @Autowired
    lateinit var leaverecordLeaveTypeRepository: LeaverecordLeaveTypeRepository

    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    lateinit var leaverecordLeaveRecordDatabaseHandler: LeaverecordLeaveRecordDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        leaverecordLeaveRecordRepository.deleteAll()
        leaverecordLeaveTypeRepository.deleteAll()
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    fun `create should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        ).toDto()

        val createdLeaveRecord = leaverecordLeaveRecordDatabaseHandler.create(leaverecord)

        assertEquals(leaverecord.date, createdLeaveRecord.date)
        assertEquals(leaverecord.isApproved, createdLeaveRecord.isApproved)

    }

    @Test
    fun `getAll should return multiple leave record`() = runBlocking {
        val maxResults: Int = 100
        val page: Int = 0

        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        )

        leaverecordLeaveRecordRepository.save(leaverecord)
        leaverecordLeaveRecordRepository.save(LeaverecordLeaveRecord(
            date = "2022-02-02",
            isApproved = false,
            employee = createdEmployee,
            leaverecord = createdLeaveType
        ))

        val leaverecords = leaverecordLeaveRecordDatabaseHandler.getAll(maxResults, page)

        assertEquals(2, leaverecords.size)

    }

    @Test
    fun `getByEmployee should return employee's leave records`() = runBlocking {
        val id: Long = 0
        val maxResults: Int = 100
        val page: Int = 0

        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        )

        leaverecordLeaveRecordRepository.save(leaverecord)
        leaverecordLeaveRecordRepository.save(LeaverecordLeaveRecord(
            date = "2022-02-02",
            isApproved = false,
            employee = createdEmployee,
            leaverecord = createdLeaveType
        ))

        val employeeRecord = leaverecordLeaveRecordDatabaseHandler.getByEmployee(id, maxResults, page)

        assertEquals(2, employeeRecord.size)

    }

    @Test
    fun `getById should return a leave record`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        )

        val createdLeaveRecord = leaverecordLeaveRecordRepository.save(leaverecord)
        val retrievedLeaveRecord = leaverecordLeaveRecordDatabaseHandler
            .getById(requireNotNull(createdLeaveRecord.id))

        requireNotNull(retrievedLeaveRecord)

        assertEquals(createdLeaveRecord.date, retrievedLeaveRecord.date)
        assertEquals(createdLeaveRecord.isApproved, retrievedLeaveRecord.isApproved)
    }

    @Test
    fun `remove should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        )

        val createdLeaveRecord = leaverecordLeaveRecordRepository.save(leaverecord)

        leaverecordLeaveRecordDatabaseHandler.remove(requireNotNull(createdLeaveRecord.id))
        assertEquals(0, leaverecordLeaveRecordRepository.findAll().size)
    }

    @Test
    fun `update should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val leaverecord = EntityGenerator.leaverecord.copy(
            employee = createdEmployee,
            leaverecord = createdLeaveType
        )

        val createdLeaveRecord = leaverecordLeaveRecordRepository.save(leaverecord)

        val body = createdLeaveRecord.copy(
            date = "2022-02-02",
        )

        val updatedLeaveRecord = leaverecordLeaveRecordDatabaseHandler.update(body.toDto(), requireNotNull(createdLeaveRecord.id))

        assertEquals(body.date, updatedLeaveRecord.date)
        assertEquals(body.isApproved, updatedLeaveRecord.isApproved)

    }
}
