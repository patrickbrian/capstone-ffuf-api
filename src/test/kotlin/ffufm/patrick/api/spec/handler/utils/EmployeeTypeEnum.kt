package ffufm.patrick.api.spec.handler.utils

enum class EmployeeTypeEnum(val value: String) {
    S("Supervisor"),
    R("Regular")
}