package ffufm.patrick.api.spec.handler.timerecord.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.repositories.project.ProjectProjectRepository
import ffufm.patrick.api.repositories.timerecord.TimerecordTimeRecordRepository
import ffufm.patrick.api.repositories.timerecord.TimerecordTimeRecordTypeRepository
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecord
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class TimerecordTimeRecordHandlerTest : PassTestBase() {
    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    lateinit var projectProjectRepository: ProjectProjectRepository

    @Autowired
    lateinit var timerecordTimeRecordTypeRepository: TimerecordTimeRecordTypeRepository

    @Autowired
    private lateinit var timerecordTimeRecordRepository: TimerecordTimeRecordRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        timerecordTimeRecordRepository.deleteAll()
        timerecordTimeRecordTypeRepository.deleteAll()
        projectProjectRepository.deleteAll()
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    @WithMockUser
    fun `create should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )
                mockMvc.post("/timerecords/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(timeRecord)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `getAll should return multiple time records`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )

        timerecordTimeRecordRepository.save(timeRecord)
        timerecordTimeRecordRepository.save(TimerecordTimeRecord(
            date = "2022-01-02",
            category = "Category 2",
            timeIn = "08:00:00",
            timeOut = "18:00:00",
            isUpdated = false,
            remarks = "Sample remarks",
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        ))
                mockMvc.get("/timerecords/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    jsonPath("$.length()") { value(2) }
                    
                }
    }

    @Test
    @WithMockUser
    fun `getByEmployee should return an employee's time records`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )

        timerecordTimeRecordRepository.save(timeRecord)
        timerecordTimeRecordRepository.save(TimerecordTimeRecord(
            date = "2022-01-02",
            category = "Category 2",
            timeIn = "08:00:00",
            timeOut = "18:00:00",
            isUpdated = false,
            remarks = "Sample remarks",
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        ))
                mockMvc.get("/employee/{id}/timerecords/", createdEmployee.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    jsonPath("$.length()") { value(2) }
                }
    }

    @Test
    @WithMockUser
    fun `getById should return a time record`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )

        val createdTimeRecord = timerecordTimeRecordRepository.save(timeRecord)

                mockMvc.get("/timerecords/{id}/", createdTimeRecord.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `remove should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )

        val createdTimeRecord = timerecordTimeRecordRepository.save(timeRecord)
                mockMvc.delete("/timerecords/{id}/", createdTimeRecord.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `update should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )

        val createdTimeRecord = timerecordTimeRecordRepository.save(timeRecord)

        val body = createdTimeRecord.copy(
            date = "2022-12-12",
            category = "Updated Category",
            timeIn = "09:00:00",
            timeOut = "20:00:00",
            isUpdated = true,
            remarks = "Updated remarks"
        )
                mockMvc.put("/timerecords/{id}/", createdTimeRecord.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }
}
