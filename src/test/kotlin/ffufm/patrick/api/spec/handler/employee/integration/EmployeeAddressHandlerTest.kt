package ffufm.patrick.api.spec.handler.employee.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.employee.EmployeeAddressRepository
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.spec.dbo.employee.EmployeeAddress
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class EmployeeAddressHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var employeeAddressRepository: EmployeeAddressRepository

    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        employeeAddressRepository.deleteAll()
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    @WithMockUser
    fun `create should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val address = EntityGenerator.address.copy(
            employee = createdEmployee
        ).toDto()
                mockMvc.post("/addresses/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(address)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `getById should return an address`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val address = EntityGenerator.address.copy(
            employee = createdEmployee
        )

        val createdAddress = employeeAddressRepository.save(address)

                mockMvc.get("/addresses/{id}/", createdAddress.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `remove should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val address = EntityGenerator.address.copy(
            employee = createdEmployee
        )

        val createdAddress = employeeAddressRepository.save(address)

                mockMvc.delete("/addresses/{id}/", createdAddress.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `update should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val address = EntityGenerator.address.copy(
            employee = createdEmployee
        )

        val createdAddress = employeeAddressRepository.save(address)

        val body = createdAddress.copy(
            street = "Updated street",
            barangay = "Updated barangay",
            city = "Updated city",
            province = "Updated province",
            zipCode = "Updated zip code"
        ).toDto()

                mockMvc.put("/addresses/{id}/", createdAddress.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }
}
