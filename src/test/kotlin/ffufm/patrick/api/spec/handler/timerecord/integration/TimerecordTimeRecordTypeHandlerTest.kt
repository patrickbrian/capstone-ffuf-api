package ffufm.patrick.api.spec.handler.timerecord.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.timerecord.TimerecordTimeRecordTypeRepository
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordType
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class TimerecordTimeRecordTypeHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var timerecordTimeRecordTypeRepository: TimerecordTimeRecordTypeRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        timerecordTimeRecordTypeRepository.deleteAll()
    }

    @Test
    @WithMockUser
    fun `create should work`() {
        val timeRecordType = EntityGenerator.timeRecordType

                mockMvc.post("/timerecordtypes/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(timeRecordType)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `getById should return a time record type`() {
        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

                mockMvc.get("/timerecordtypes/{id}/", createdTimeRecordType.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `remove shoudl work`() {
        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

                mockMvc.delete("/timerecordtypes/{id}/", createdTimeRecordType.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `update should work`() {
        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val body = createdTimeRecordType.copy(
            shortName = "WP",
            longName = "Waiting Period",
            numberOfHours = 2
        )

                mockMvc.put("/timerecordtypes/{id}/", createdTimeRecordType.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }
}
