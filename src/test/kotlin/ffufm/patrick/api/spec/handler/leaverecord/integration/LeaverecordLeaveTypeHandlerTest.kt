package ffufm.patrick.api.spec.handler.leaverecord.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.leaverecord.LeaverecordLeaveTypeRepository
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveType
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class LeaverecordLeaveTypeHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var leaverecordLeaveTypeRepository: LeaverecordLeaveTypeRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        leaverecordLeaveTypeRepository.deleteAll()
    }

    @Test
    @WithMockUser
    fun `create should work`() {

        val leaveType = EntityGenerator.leaveType
                mockMvc.post("/leavetypes/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(leaveType)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `getById should return a leave type`() {
        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)
                mockMvc.get("/leavetypes/{id}/", createdLeaveType.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `remove should work`() {
        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)
                mockMvc.delete("/leavetypes/{id}/", createdLeaveType.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `update should work`() {
        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val body = createdLeaveType.copy(
            shortName = "VL",
            maximumAllowed = 5,
            longName = "Vacation Leave"
        ).toDto()
                mockMvc.put("/leavetypes/{id}/", createdLeaveType.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }
}
