package ffufm.patrick.api.spec.handler.timerecord.implementation

import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.repositories.project.ProjectProjectRepository
import ffufm.patrick.api.repositories.timerecord.TimerecordTimeRecordRepository
import ffufm.patrick.api.repositories.timerecord.TimerecordTimeRecordTypeRepository
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecord
import ffufm.patrick.api.spec.handler.timerecord.TimerecordTimeRecordDatabaseHandler
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class TimerecordTimeRecordDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    lateinit var projectProjectRepository: ProjectProjectRepository

    @Autowired
    lateinit var timerecordTimeRecordTypeRepository: TimerecordTimeRecordTypeRepository

    @Autowired
    lateinit var timerecordTimeRecordRepository: TimerecordTimeRecordRepository

    @Autowired
    lateinit var timerecordTimeRecordDatabaseHandler: TimerecordTimeRecordDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        timerecordTimeRecordRepository.deleteAll()
        timerecordTimeRecordTypeRepository.deleteAll()
        projectProjectRepository.deleteAll()
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    fun `create should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )
        val createdTimeRecord = timerecordTimeRecordDatabaseHandler.create(timeRecord.toDto())

        assertEquals(timeRecord.date, createdTimeRecord.date)
        assertEquals(timeRecord.category, createdTimeRecord.category)
        assertEquals(timeRecord.timeIn, createdTimeRecord.timeIn)
        assertEquals(timeRecord.timeOut, createdTimeRecord.timeOut)
        assertEquals(timeRecord.isUpdated, createdTimeRecord.isUpdated)
        assertEquals(timeRecord.remarks, createdTimeRecord.remarks)

    }

    @Test
    fun `getAll should return multiple time records`() = runBlocking {
        val maxResults: Int = 100
        val page: Int = 0

        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )

        timerecordTimeRecordRepository.save(timeRecord)
        timerecordTimeRecordRepository.save(TimerecordTimeRecord(
            date = "2022-01-02",
            category = "Category 2",
            timeIn = "08:00:00",
            timeOut = "18:00:00",
            isUpdated = false,
            remarks = "Sample remarks",
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        ))

        val retrievedTimeRecords = timerecordTimeRecordDatabaseHandler.getAll(maxResults, page)
        assertEquals(2, retrievedTimeRecords.size)
    }

    @Test
    fun `getByEmployee should return an employee's time records`() = runBlocking {
        val maxResults: Int = 100
        val page: Int = 0

        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )

        timerecordTimeRecordRepository.save(timeRecord)
        timerecordTimeRecordRepository.save(TimerecordTimeRecord(
            date = "2022-01-02",
            category = "Category 2",
            timeIn = "08:00:00",
            timeOut = "18:00:00",
            isUpdated = false,
            remarks = "Sample remarks",
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        ))


        val employeeTimeRecords = timerecordTimeRecordDatabaseHandler.getByEmployee(
            requireNotNull(createdEmployee.id), maxResults, page)

        assertEquals(2, employeeTimeRecords.size)
    }

    @Test
    fun `getById should return a time record`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )

        val createdTimeRecord = timerecordTimeRecordRepository.save(timeRecord)

        val retrievedTimeRecord = timerecordTimeRecordDatabaseHandler
            .getById(requireNotNull(createdTimeRecord.id))

        requireNotNull(retrievedTimeRecord)

        assertEquals(createdTimeRecord.date, retrievedTimeRecord.date)
        assertEquals(createdTimeRecord.category, retrievedTimeRecord.category)
        assertEquals(createdTimeRecord.timeIn, retrievedTimeRecord.timeIn)
        assertEquals(createdTimeRecord.timeOut, retrievedTimeRecord.timeOut)
        assertEquals(createdTimeRecord.isUpdated, retrievedTimeRecord.isUpdated)
        assertEquals(createdTimeRecord.remarks, retrievedTimeRecord.remarks)
    }

    @Test
    fun `remove should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )
        val createdTimeRecord = timerecordTimeRecordRepository.save(timeRecord)

        timerecordTimeRecordDatabaseHandler.remove(requireNotNull(createdTimeRecord.id))

        assertEquals(0, timerecordTimeRecordRepository.findAll().size)
    }

    @Test
    fun `update should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy(employee = createdEmployee)
        val createdProject = projectProjectRepository.save(project)

        val timeRecordType = EntityGenerator.timeRecordType
        val createdTimeRecordType = timerecordTimeRecordTypeRepository.save(timeRecordType)

        val timeRecord = EntityGenerator.timeRecord.copy(
            employee = createdEmployee,
            timerecord = createdTimeRecordType,
            projectTimerecord = createdProject
        )

        val createdTimeRecord = timerecordTimeRecordRepository.save(timeRecord)

        val body = createdTimeRecord.copy(
            date = "2022-12-12",
            category = "Updated Category",
            timeIn = "09:00:00",
            timeOut = "20:00:00",
            isUpdated = true,
            remarks = "Updated remarks",
        )
        val updatedTimeRecord = timerecordTimeRecordDatabaseHandler
            .update(body.toDto(), requireNotNull(createdTimeRecord.id))

        assertEquals(body.date, updatedTimeRecord.date)
        assertEquals(body.category, updatedTimeRecord.category)
        assertEquals(body.timeIn, updatedTimeRecord.timeIn)
        assertEquals(body.timeOut, updatedTimeRecord.timeOut)
        assertEquals(body.isUpdated, updatedTimeRecord.isUpdated)
        assertEquals(body.remarks, updatedTimeRecord.remarks)

    }
}
