package ffufm.patrick.api.spec.handler.project.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.repositories.project.ProjectProjectRepository
import ffufm.patrick.api.spec.dbo.project.ProjectProject
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class ProjectProjectHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var projectProjectRepository: ProjectProjectRepository

    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        projectProjectRepository.deleteAll()
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    @WithMockUser
    fun `create should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy( employee = createdEmployee )
                mockMvc.post("/projects/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(project)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `getByEmployee should return employee's project`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy( employee = createdEmployee )
        projectProjectRepository.saveAll(
            listOf(
                project,
                ProjectProject(
                    name = "Project 2",
                    description = "Description of Project 2",
                    employee = createdEmployee
                )
            )
        )


                mockMvc.get("/employee/{id}/projects/", createdEmployee.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    jsonPath("$.length()") { value(2) }
                    
                }
    }

    @Test
    @WithMockUser
    fun `getById should return a project`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy( employee = createdEmployee )
        val createdProject = projectProjectRepository.save(project)

                mockMvc.get("/projects/{id}/", createdProject.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `remove should work`() {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy( employee = createdEmployee )
        val createdProject = projectProjectRepository.save(project)

        mockMvc.delete("/projects/{id}/", createdProject.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `update should work`() {

        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val project = EntityGenerator.project.copy( employee = createdEmployee )
        val createdProject = projectProjectRepository.save(project)

        val body = createdProject.copy(
            name = "Updated Project Name",
            description = "Updated Project Description"
        ).toDto()
                mockMvc.put("/projects/{id}/", createdProject.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.asyncDispatch().andExpect {
                    status { isOk() }
                    
                }
    }
}
