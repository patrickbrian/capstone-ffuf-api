package ffufm.patrick.api.spec.handler.employee.implementation

import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.employee.EmployeeAddressRepository
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.spec.dbo.employee.EmployeeAddress
import ffufm.patrick.api.spec.handler.employee.EmployeeAddressDatabaseHandler
import ffufm.patrick.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class EmployeeAddressDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var employeeAddressRepository: EmployeeAddressRepository

    @Autowired
    lateinit var employeeAddressDatabaseHandler: EmployeeAddressDatabaseHandler

    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository


    @Before
    @After
    fun cleanRepositories() {
        employeeAddressRepository.deleteAll()
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    fun `create should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val address = EntityGenerator.address.copy(
            employee = createdEmployee
        ).toDto()

        val createdAddress = employeeAddressDatabaseHandler.create(address)

        assertEquals(address.street, createdAddress.street)
        assertEquals(address.barangay, createdAddress.barangay)
        assertEquals(address.city, createdAddress.city)
        assertEquals(address.province, createdAddress.province)
        assertEquals(address.zipCode, createdAddress.zipCode)
    }

    @Test
    fun `getById should return an address`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val address = EntityGenerator.address.copy(
            employee = createdEmployee
        )

        val createdAddress = employeeAddressRepository.save(address)

        val retrievedAddress = employeeAddressDatabaseHandler.getById(requireNotNull(createdAddress.id))

        requireNotNull(retrievedAddress)

        assertEquals(createdAddress.street, retrievedAddress.street)
        assertEquals(createdAddress.barangay, retrievedAddress.barangay)
        assertEquals(createdAddress.city, retrievedAddress.city)
        assertEquals(createdAddress.province, retrievedAddress.province)
        assertEquals(createdAddress.zipCode, retrievedAddress.zipCode)

    }

    @Test
    fun `remove should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val address = EntityGenerator.address.copy(
            employee = createdEmployee
        )

        val createdAddress = employeeAddressRepository.save(address)

        employeeAddressDatabaseHandler.remove(requireNotNull(createdAddress.id))

        assertEquals(0, employeeAddressRepository.findAll().size)
    }

    @Test
    fun `update should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val address = EntityGenerator.address.copy(
            employee = createdEmployee
        )

        val createdAddress = employeeAddressRepository.save(address)

        val body = createdAddress.copy(
            street = "Updated street",
            barangay = "Updated barangay",
            city = "Updated city",
            province = "Updated province",
            zipCode = "Updated zip code"
        ).toDto()

        val updatedAddress = employeeAddressDatabaseHandler
            .update(body, requireNotNull(createdAddress.id))

        assertEquals(body.street, updatedAddress.street)
        assertEquals(body.barangay, updatedAddress.barangay)
        assertEquals(body.city, updatedAddress.city)
        assertEquals(body.province, updatedAddress.province)
        assertEquals(body.zipCode, updatedAddress.zipCode)
    }
}
