package ffufm.patrick.api.spec.handler.leaverecord.implementation

import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.leaverecord.LeaverecordLeaveTypeRepository
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveType
import ffufm.patrick.api.spec.handler.leaverecord.LeaverecordLeaveTypeDatabaseHandler
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class LeaverecordLeaveTypeDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var leaverecordLeaveTypeRepository: LeaverecordLeaveTypeRepository

    @Autowired
    lateinit var leaverecordLeaveTypeDatabaseHandler: LeaverecordLeaveTypeDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        leaverecordLeaveTypeRepository.deleteAll()
    }

    @Test
    fun `create should work`() = runBlocking {
        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeDatabaseHandler.create(leaveType.toDto())

        assertEquals(leaveType.shortName, createdLeaveType.shortName)
        assertEquals(leaveType.maximumAllowed, createdLeaveType.maximumAllowed)
        assertEquals(leaveType.longName, createdLeaveType.longName)

    }

    @Test
    fun `getById should return a leave type`() = runBlocking {
        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val retrievedLeaveType = leaverecordLeaveTypeDatabaseHandler
            .getById(requireNotNull(createdLeaveType.id))

        requireNotNull(retrievedLeaveType)

        assertEquals(createdLeaveType.shortName, retrievedLeaveType.shortName)
        assertEquals(createdLeaveType.maximumAllowed, retrievedLeaveType.maximumAllowed)
        assertEquals(createdLeaveType.longName, retrievedLeaveType.longName)

    }

    @Test
    fun `remove should work`() = runBlocking {
        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)
        leaverecordLeaveTypeDatabaseHandler.remove(requireNotNull(createdLeaveType.id))

        assertEquals(0, leaverecordLeaveTypeRepository.findAll().size)

    }

    @Test
    fun `update should work`() = runBlocking {
        val leaveType = EntityGenerator.leaveType
        val createdLeaveType = leaverecordLeaveTypeRepository.save(leaveType)

        val body = createdLeaveType.copy(
            shortName = "VL",
            maximumAllowed = 5,
            longName = "Vacation Leave"
        ).toDto()

        val updatedLeaveType = leaverecordLeaveTypeDatabaseHandler
            .update(body, requireNotNull(createdLeaveType.id))

        assertEquals(body.shortName, updatedLeaveType.shortName)
        assertEquals(body.maximumAllowed, updatedLeaveType.maximumAllowed)
        assertEquals(body.longName, updatedLeaveType.longName)
    }
}
