package ffufm.patrick.api.spec.handler.employee.implementation

import ffufm.patrick.api.PassTestBase
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployee
import ffufm.patrick.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import ffufm.patrick.api.spec.handler.utils.EmployeeTypeEnum
import ffufm.patrick.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class EmployeeEmployeeDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    lateinit var employeeEmployeeDatabaseHandler: EmployeeEmployeeDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        employeeEmployeeRepository.deleteAll()
    }

    @Test
    fun `create should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeDatabaseHandler.create(employee.toDto())


        assertEquals(employee.firstName, createdEmployee.firstName)
        assertEquals(employee.lastName, createdEmployee.lastName)
        assertEquals(employee.email, createdEmployee.email)
        assertEquals(employee.employeeType, createdEmployee.employeeType)

    }

    @Test
    fun `getAll should return multiple employees`() = runBlocking {
        val maxResults: Int = 100
        val page: Int = 0

        val employee = EntityGenerator.employee
        employeeEmployeeRepository.save(employee)
        employeeEmployeeRepository.save(EmployeeEmployee(
            firstName = "Brenda",
            lastName = "Mage",
            email = "brenda@email.com",
            employeeType = EmployeeTypeEnum.R.name
        ))

        val employees = employeeEmployeeDatabaseHandler.getAll(maxResults, page)

        assertEquals(2, employees.size)
    }

    @Test
    fun `getById should return an employee`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val retrievedEmployee = employeeEmployeeDatabaseHandler
            .getById(requireNotNull(createdEmployee.id))

        requireNotNull(retrievedEmployee)

        assertEquals(createdEmployee.firstName, retrievedEmployee.firstName)
        assertEquals(createdEmployee.lastName, retrievedEmployee.lastName)
        assertEquals(createdEmployee.email, retrievedEmployee.email)
        assertEquals(createdEmployee.employeeType, retrievedEmployee.employeeType)
    }

    @Test
    fun `remove should work`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)


        employeeEmployeeDatabaseHandler.remove(requireNotNull(createdEmployee.id))

        assertEquals(0, employeeEmployeeRepository.findAll().size)

    }

    @Test
    fun `update should return updated valid inputs`() = runBlocking {
        val employee = EntityGenerator.employee
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val body = createdEmployee.copy(
            firstName = "Brenda",
            lastName = "Mage",
            email = "brenda@email.com",
            employeeType = EmployeeTypeEnum.R.name
        ).toDto()

        val updatedEmployee = employeeEmployeeDatabaseHandler
            .update(body, requireNotNull(createdEmployee.id))

        assertEquals(body.firstName, updatedEmployee.firstName)
        assertEquals(body.lastName, updatedEmployee.lastName)
        assertEquals(body.email, updatedEmployee.email)
        assertEquals(body.employeeType, updatedEmployee.employeeType)
    }
}
