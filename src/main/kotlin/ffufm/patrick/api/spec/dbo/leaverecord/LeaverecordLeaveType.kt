package ffufm.patrick.api.spec.dbo.leaverecord

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveRecordSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.OneToMany
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Int
import kotlin.Long
import kotlin.String
import kotlin.collections.List
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * Type of the leave record
 */
@Entity(name = "LeaverecordLeaveType")
@Table(name = "leaverecord_leavetype")
data class LeaverecordLeaveType(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Short name of the leave type
     * Sample: SL
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "short_name"
    )
    @Lob
    val shortName: String = "",
    /**
     * Maximum allowed of leave
     * Sample: 2
     */
    @Column(name = "maximum_allowed")
    val maximumAllowed: Int = 0,
    /**
     * Long name of the leave type
     * Sample: Sick Leave
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "long_name"
    )
    @Lob
    val longName: String = "",
    @OneToMany(mappedBy = "leaverecord", fetch = FetchType.LAZY)
    val leaveRecords: List<LeaverecordLeaveRecord>? = mutableListOf()
) : PassDTOModel<LeaverecordLeaveType, LeaverecordLeaveTypeDTO, Long>() {
    override fun toDto(): LeaverecordLeaveTypeDTO =
            super.toDtoInternal(LeaverecordLeaveTypeSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<LeaverecordLeaveType, LeaverecordLeaveTypeDTO,
            Long>, LeaverecordLeaveTypeDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Type of the leave record
 */
data class LeaverecordLeaveTypeDTO(
    val id: Long? = null,
    /**
     * Short name of the leave type
     * Sample: SL
     */
    val shortName: String? = "",
    /**
     * Maximum allowed of leave
     * Sample: 2
     */
    val maximumAllowed: Int? = 0,
    /**
     * Long name of the leave type
     * Sample: Sick Leave
     */
    val longName: String? = "",
    val leaveRecords: List<LeaverecordLeaveRecordDTO>? = null
) : PassDTO<LeaverecordLeaveType, Long>() {
    override fun toEntity(): LeaverecordLeaveType =
            super.toEntityInternal(LeaverecordLeaveTypeSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<LeaverecordLeaveType,
            PassDTO<LeaverecordLeaveType, Long>, Long>, PassDTO<LeaverecordLeaveType, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class LeaverecordLeaveTypeSerializer : PassDtoSerializer<LeaverecordLeaveType,
        LeaverecordLeaveTypeDTO, Long>() {
    override fun toDto(entity: LeaverecordLeaveType): LeaverecordLeaveTypeDTO = cycle(entity) {
        LeaverecordLeaveTypeDTO(
                id = entity.id,
        shortName = entity.shortName,
        maximumAllowed = entity.maximumAllowed,
        longName = entity.longName,
        leaveRecords = entity.leaveRecords?.toSafeDtos()
                )}

    override fun toEntity(dto: LeaverecordLeaveTypeDTO): LeaverecordLeaveType =
            LeaverecordLeaveType(
            id = dto.id,
    shortName = dto.shortName ?: "",
    maximumAllowed = dto.maximumAllowed ?: 0,
    longName = dto.longName ?: "",
    leaveRecords = dto.leaveRecords?.toEntities() ?: emptyList()
            )
    override fun idDto(id: Long): LeaverecordLeaveTypeDTO = LeaverecordLeaveTypeDTO(
            id = id,
    shortName = null,
    maximumAllowed = null,
    longName = null,

            )}

@Service("leaverecord.LeaverecordLeaveTypeValidator")
class LeaverecordLeaveTypeValidator : PassModelValidation<LeaverecordLeaveType> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<LeaverecordLeaveType>):
            ValidatorBuilder<LeaverecordLeaveType> = validatorBuilder.apply {
    }
}

@Service("leaverecord.LeaverecordLeaveTypeDTOValidator")
class LeaverecordLeaveTypeDTOValidator : PassModelValidation<LeaverecordLeaveTypeDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<LeaverecordLeaveTypeDTO>):
            ValidatorBuilder<LeaverecordLeaveTypeDTO> = validatorBuilder.apply {
    }
}
