package ffufm.patrick.api.spec.dbo.employee

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployeeSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * This contains the address of the employee
 */
@Entity(name = "EmployeeAddress")
@Table(name = "employee_address")
data class EmployeeAddress(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Street of the address
     * Sample: Pureza
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "street"
    )
    @Lob
    val street: String = "",
    /**
     * Barangay of the address
     * Sample: Sta. Mesa
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "barangay"
    )
    @Lob
    val barangay: String = "",
    /**
     * City of the address
     * Sample: Manila
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "city"
    )
    @Lob
    val city: String = "",
    /**
     * Province of the address
     * Sample: Metro Manila
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "province"
    )
    @Lob
    val province: String = "",
    /**
     * Zip code of the address
     * Sample: 1016
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "zip_code"
    )
    @Lob
    val zipCode: String = "",
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val employee: EmployeeEmployee? = null
) : PassDTOModel<EmployeeAddress, EmployeeAddressDTO, Long>() {
    override fun toDto(): EmployeeAddressDTO = super.toDtoInternal(EmployeeAddressSerializer::class
            as KClass<PassDtoSerializer<PassDTOModel<EmployeeAddress, EmployeeAddressDTO, Long>,
            EmployeeAddressDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * This contains the address of the employee
 */
data class EmployeeAddressDTO(
    val id: Long? = null,
    /**
     * Street of the address
     * Sample: Pureza
     */
    val street: String? = "",
    /**
     * Barangay of the address
     * Sample: Sta. Mesa
     */
    val barangay: String? = "",
    /**
     * City of the address
     * Sample: Manila
     */
    val city: String? = "",
    /**
     * Province of the address
     * Sample: Metro Manila
     */
    val province: String? = "",
    /**
     * Zip code of the address
     * Sample: 1016
     */
    val zipCode: String? = "",
    val employee: EmployeeEmployeeDTO? = null
) : PassDTO<EmployeeAddress, Long>() {
    override fun toEntity(): EmployeeAddress =
            super.toEntityInternal(EmployeeAddressSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<EmployeeAddress, PassDTO<EmployeeAddress, Long>,
            Long>, PassDTO<EmployeeAddress, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class EmployeeAddressSerializer : PassDtoSerializer<EmployeeAddress, EmployeeAddressDTO, Long>() {
    override fun toDto(entity: EmployeeAddress): EmployeeAddressDTO = cycle(entity) {
        EmployeeAddressDTO(
                id = entity.id,
        street = entity.street,
        barangay = entity.barangay,
        city = entity.city,
        province = entity.province,
        zipCode = entity.zipCode,
        employee = entity.employee?.idDto() ?: entity.employee?.toDto()
                )}

    override fun toEntity(dto: EmployeeAddressDTO): EmployeeAddress = EmployeeAddress(
            id = dto.id,
    street = dto.street ?: "",
    barangay = dto.barangay ?: "",
    city = dto.city ?: "",
    province = dto.province ?: "",
    zipCode = dto.zipCode ?: "",
    employee = dto.employee?.toEntity()
            )
    override fun idDto(id: Long): EmployeeAddressDTO = EmployeeAddressDTO(
            id = id,
    street = null,
    barangay = null,
    city = null,
    province = null,
    zipCode = null,

            )}

@Service("employee.EmployeeAddressValidator")
class EmployeeAddressValidator : PassModelValidation<EmployeeAddress> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeAddress>):
            ValidatorBuilder<EmployeeAddress> = validatorBuilder.apply {
        konstraintOnObject(EmployeeAddress::employee) {
            notNull()
        }
    }
}

@Service("employee.EmployeeAddressDTOValidator")
class EmployeeAddressDTOValidator : PassModelValidation<EmployeeAddressDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeAddressDTO>):
            ValidatorBuilder<EmployeeAddressDTO> = validatorBuilder.apply {
        konstraintOnObject(EmployeeAddressDTO::employee) {
            notNull()
        }
    }
}
