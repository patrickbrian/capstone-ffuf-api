package ffufm.patrick.api.spec.dbo.timerecord

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.OneToMany
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Int
import kotlin.Long
import kotlin.String
import kotlin.collections.List
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * Type of the time record
 */
@Entity(name = "TimerecordTimeRecordType")
@Table(name = "timerecord_timerecordtype")
data class TimerecordTimeRecordType(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Short name of the time record type
     * Sample: WH
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "short_name"
    )
    @Lob
    val shortName: String = "",
    /**
     * Long name of the time record type
     * Sample: Working Hours
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "long_name"
    )
    @Lob
    val longName: String = "",
    /**
     * Number of hours for each time record type
     * Sample: 2
     */
    @Column(name = "number_of_hours")
    val numberOfHours: Int = 0,
    @OneToMany(mappedBy = "timerecord", fetch = FetchType.LAZY)
    val timeRecords: List<TimerecordTimeRecord>? = mutableListOf()
) : PassDTOModel<TimerecordTimeRecordType, TimerecordTimeRecordTypeDTO, Long>() {
    override fun toDto(): TimerecordTimeRecordTypeDTO =
            super.toDtoInternal(TimerecordTimeRecordTypeSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TimerecordTimeRecordType,
            TimerecordTimeRecordTypeDTO, Long>, TimerecordTimeRecordTypeDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Type of the time record
 */
data class TimerecordTimeRecordTypeDTO(
    val id: Long? = null,
    /**
     * Short name of the time record type
     * Sample: WH
     */
    val shortName: String? = "",
    /**
     * Long name of the time record type
     * Sample: Working Hours
     */
    val longName: String? = "",
    /**
     * Number of hours for each time record type
     * Sample: 2
     */
    val numberOfHours: Int? = 0,
    val timeRecords: List<TimerecordTimeRecordDTO>? = null
) : PassDTO<TimerecordTimeRecordType, Long>() {
    override fun toEntity(): TimerecordTimeRecordType =
            super.toEntityInternal(TimerecordTimeRecordTypeSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TimerecordTimeRecordType,
            PassDTO<TimerecordTimeRecordType, Long>, Long>, PassDTO<TimerecordTimeRecordType, Long>,
            Long>>)

    override fun readId(): Long? = this.id
}

@Component
class TimerecordTimeRecordTypeSerializer : PassDtoSerializer<TimerecordTimeRecordType,
        TimerecordTimeRecordTypeDTO, Long>() {
    override fun toDto(entity: TimerecordTimeRecordType): TimerecordTimeRecordTypeDTO =
            cycle(entity) {
        TimerecordTimeRecordTypeDTO(
                id = entity.id,
        shortName = entity.shortName,
        longName = entity.longName,
        numberOfHours = entity.numberOfHours,
        timeRecords = entity.timeRecords?.toSafeDtos()
                )}

    override fun toEntity(dto: TimerecordTimeRecordTypeDTO): TimerecordTimeRecordType =
            TimerecordTimeRecordType(
            id = dto.id,
    shortName = dto.shortName ?: "",
    longName = dto.longName ?: "",
    numberOfHours = dto.numberOfHours ?: 0,
    timeRecords = dto.timeRecords?.toEntities() ?: emptyList()
            )
    override fun idDto(id: Long): TimerecordTimeRecordTypeDTO = TimerecordTimeRecordTypeDTO(
            id = id,
    shortName = null,
    longName = null,
    numberOfHours = null,

            )}

@Service("timerecord.TimerecordTimeRecordTypeValidator")
class TimerecordTimeRecordTypeValidator : PassModelValidation<TimerecordTimeRecordType> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TimerecordTimeRecordType>):
            ValidatorBuilder<TimerecordTimeRecordType> = validatorBuilder.apply {
    }
}

@Service("timerecord.TimerecordTimeRecordTypeDTOValidator")
class TimerecordTimeRecordTypeDTOValidator : PassModelValidation<TimerecordTimeRecordTypeDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TimerecordTimeRecordTypeDTO>):
            ValidatorBuilder<TimerecordTimeRecordTypeDTO> = validatorBuilder.apply {
    }
}
