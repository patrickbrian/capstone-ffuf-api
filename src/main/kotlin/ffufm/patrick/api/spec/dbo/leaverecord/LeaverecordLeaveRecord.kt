package ffufm.patrick.api.spec.dbo.leaverecord

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployee
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployeeDTO
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployeeSerializer
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveTypeSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Boolean
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * This contains the employee&#039;s leave record model
 */
@Entity(name = "LeaverecordLeaveRecord")
@Table(name = "leaverecord_leaverecord")
data class LeaverecordLeaveRecord(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Date of the of leave record
     * Sample: 2022-02-01
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "date"
    )
    @Lob
    val date: String = "",
    /**
     * Approve status of the leave record
     * Sample: FALSE
     */
    @Column(name = "is_approved")
    val isApproved: Boolean = false,
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val employee: EmployeeEmployee? = null,
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val leaverecord: LeaverecordLeaveType? = null
) : PassDTOModel<LeaverecordLeaveRecord, LeaverecordLeaveRecordDTO, Long>() {
    override fun toDto(): LeaverecordLeaveRecordDTO =
            super.toDtoInternal(LeaverecordLeaveRecordSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<LeaverecordLeaveRecord, LeaverecordLeaveRecordDTO,
            Long>, LeaverecordLeaveRecordDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * This contains the employee&#039;s leave record model
 */
data class LeaverecordLeaveRecordDTO(
    val id: Long? = null,
    /**
     * Date of the of leave record
     * Sample: 2022-02-01
     */
    val date: String? = "",
    /**
     * Approve status of the leave record
     * Sample: FALSE
     */
    val isApproved: Boolean? = false,
    val employee: EmployeeEmployeeDTO? = null,
    val leaverecord: LeaverecordLeaveTypeDTO? = null
) : PassDTO<LeaverecordLeaveRecord, Long>() {
    override fun toEntity(): LeaverecordLeaveRecord =
            super.toEntityInternal(LeaverecordLeaveRecordSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<LeaverecordLeaveRecord,
            PassDTO<LeaverecordLeaveRecord, Long>, Long>, PassDTO<LeaverecordLeaveRecord, Long>,
            Long>>)

    override fun readId(): Long? = this.id
}

@Component
class LeaverecordLeaveRecordSerializer : PassDtoSerializer<LeaverecordLeaveRecord,
        LeaverecordLeaveRecordDTO, Long>() {
    override fun toDto(entity: LeaverecordLeaveRecord): LeaverecordLeaveRecordDTO = cycle(entity) {
        LeaverecordLeaveRecordDTO(
                id = entity.id,
        date = entity.date,
        isApproved = entity.isApproved,
        employee = entity.employee?.idDto() ?: entity.employee?.toDto(),
        leaverecord = entity.leaverecord?.idDto() ?: entity.leaverecord?.toDto()
                )}

    override fun toEntity(dto: LeaverecordLeaveRecordDTO): LeaverecordLeaveRecord =
            LeaverecordLeaveRecord(
            id = dto.id,
    date = dto.date ?: "",
    isApproved = dto.isApproved ?: false,
    employee = dto.employee?.toEntity(),
    leaverecord = dto.leaverecord?.toEntity()
            )
    override fun idDto(id: Long): LeaverecordLeaveRecordDTO = LeaverecordLeaveRecordDTO(
            id = id,
    date = null,
    isApproved = null,

            )}

@Service("leaverecord.LeaverecordLeaveRecordValidator")
class LeaverecordLeaveRecordValidator : PassModelValidation<LeaverecordLeaveRecord> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<LeaverecordLeaveRecord>):
            ValidatorBuilder<LeaverecordLeaveRecord> = validatorBuilder.apply {
        konstraintOnObject(LeaverecordLeaveRecord::employee) {
            notNull()
        }
        konstraintOnObject(LeaverecordLeaveRecord::leaverecord) {
            notNull()
        }
    }
}

@Service("leaverecord.LeaverecordLeaveRecordDTOValidator")
class LeaverecordLeaveRecordDTOValidator : PassModelValidation<LeaverecordLeaveRecordDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<LeaverecordLeaveRecordDTO>):
            ValidatorBuilder<LeaverecordLeaveRecordDTO> = validatorBuilder.apply {
        konstraintOnObject(LeaverecordLeaveRecordDTO::employee) {
            notNull()
        }
        konstraintOnObject(LeaverecordLeaveRecordDTO::leaverecord) {
            notNull()
        }
    }
}
