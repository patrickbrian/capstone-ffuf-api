package ffufm.patrick.api.spec.dbo.employee

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.patrick.api.spec.dbo.employee.EmployeeAddressSerializer
import ffufm.patrick.api.spec.dbo.employee.EmployeeContactDetailSerializer
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveRecordDTO
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveRecordSerializer
import ffufm.patrick.api.spec.dbo.project.ProjectProject
import ffufm.patrick.api.spec.dbo.project.ProjectProjectDTO
import ffufm.patrick.api.spec.dbo.project.ProjectProjectSerializer
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecord
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordDTO
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.OneToMany
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import kotlin.collections.List
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * This contains the employee model
 */
@Entity(name = "EmployeeEmployee")
@Table(name = "employee_employee")
data class EmployeeEmployee(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * First name of the employee
     * Sample: Brandon
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "first_name"
    )
    @Lob
    val firstName: String = "",
    /**
     * Last name of the employee
     * Sample: Cruz
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "last_name"
    )
    @Lob
    val lastName: String = "",
    /**
     * Email of the employee
     * Sample: brandon@email.com
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "email"
    )
    @Lob
    val email: String = "",
    /**
     * Type of the employee
     * Sample: S
     */
    @Column(
        length = 1,
        updatable = true,
        nullable = false,
        name = "employee_type"
    )
    val employeeType: String = "",
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val addresses: List<EmployeeAddress>? = mutableListOf(),
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val contactDetails: List<EmployeeContactDetail>? = mutableListOf(),
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val leaveRecords: List<LeaverecordLeaveRecord>? = mutableListOf(),
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val timeRecords: List<TimerecordTimeRecord>? = mutableListOf(),
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val projects: List<ProjectProject>? = mutableListOf()
) : PassDTOModel<EmployeeEmployee, EmployeeEmployeeDTO, Long>() {
    override fun toDto(): EmployeeEmployeeDTO =
            super.toDtoInternal(EmployeeEmployeeSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<EmployeeEmployee, EmployeeEmployeeDTO, Long>,
            EmployeeEmployeeDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * This contains the employee model
 */
data class EmployeeEmployeeDTO(
    val id: Long? = null,
    /**
     * First name of the employee
     * Sample: Brandon
     */
    val firstName: String? = "",
    /**
     * Last name of the employee
     * Sample: Cruz
     */
    val lastName: String? = "",
    /**
     * Email of the employee
     * Sample: brandon@email.com
     */
    val email: String? = "",
    /**
     * Type of the employee
     * Sample: S
     */
    val employeeType: String? = "",
    val addresses: List<EmployeeAddressDTO>? = null,
    val contactDetails: List<EmployeeContactDetailDTO>? = null,
    val leaveRecords: List<LeaverecordLeaveRecordDTO>? = null,
    val timeRecords: List<TimerecordTimeRecordDTO>? = null,
    val projects: List<ProjectProjectDTO>? = null
) : PassDTO<EmployeeEmployee, Long>() {
    override fun toEntity(): EmployeeEmployee =
            super.toEntityInternal(EmployeeEmployeeSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<EmployeeEmployee, PassDTO<EmployeeEmployee, Long>,
            Long>, PassDTO<EmployeeEmployee, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class EmployeeEmployeeSerializer : PassDtoSerializer<EmployeeEmployee, EmployeeEmployeeDTO, Long>()
        {
    override fun toDto(entity: EmployeeEmployee): EmployeeEmployeeDTO = cycle(entity) {
        EmployeeEmployeeDTO(
                id = entity.id,
        firstName = entity.firstName,
        lastName = entity.lastName,
        email = entity.email,
        employeeType = entity.employeeType,
        addresses = entity.addresses?.toSafeDtos(),
        contactDetails = entity.contactDetails?.toSafeDtos(),
        leaveRecords = entity.leaveRecords?.toSafeDtos(),
        timeRecords = entity.timeRecords?.toSafeDtos(),
        projects = entity.projects?.toSafeDtos()
                )}

    override fun toEntity(dto: EmployeeEmployeeDTO): EmployeeEmployee = EmployeeEmployee(
            id = dto.id,
    firstName = dto.firstName ?: "",
    lastName = dto.lastName ?: "",
    email = dto.email ?: "",
    employeeType = dto.employeeType ?: "",
    addresses = dto.addresses?.toEntities() ?: emptyList(),
    contactDetails = dto.contactDetails?.toEntities() ?: emptyList(),
    leaveRecords = dto.leaveRecords?.toEntities() ?: emptyList(),
    timeRecords = dto.timeRecords?.toEntities() ?: emptyList(),
    projects = dto.projects?.toEntities() ?: emptyList()
            )
    override fun idDto(id: Long): EmployeeEmployeeDTO = EmployeeEmployeeDTO(
            id = id,
    firstName = null,
    lastName = null,
    email = null,
    employeeType = null,

            )}

@Service("employee.EmployeeEmployeeValidator")
class EmployeeEmployeeValidator : PassModelValidation<EmployeeEmployee> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeEmployee>):
            ValidatorBuilder<EmployeeEmployee> = validatorBuilder.apply {
        konstraint(EmployeeEmployee::employeeType) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(1)
        }
    }
}

@Service("employee.EmployeeEmployeeDTOValidator")
class EmployeeEmployeeDTOValidator : PassModelValidation<EmployeeEmployeeDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeEmployeeDTO>):
            ValidatorBuilder<EmployeeEmployeeDTO> = validatorBuilder.apply {
        konstraint(EmployeeEmployeeDTO::employeeType) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(1)
        }
    }
}
