package ffufm.patrick.api.spec.dbo.employee

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployeeSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * This contains the contact details of the employee
 */
@Entity(name = "EmployeeContactDetail")
@Table(name = "employee_contactdetail")
data class EmployeeContactDetail(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Contact details of the employee&#039;s contact
     * Sample: 09123456789
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "contact_details"
    )
    @Lob
    val contactDetails: String = "",
    /**
     * Type of employee&#039;s contact
     * Sample: Mobile
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "contact_type"
    )
    @Lob
    val contactType: String = "",
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val employee: EmployeeEmployee? = null
) : PassDTOModel<EmployeeContactDetail, EmployeeContactDetailDTO, Long>() {
    override fun toDto(): EmployeeContactDetailDTO =
            super.toDtoInternal(EmployeeContactDetailSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<EmployeeContactDetail, EmployeeContactDetailDTO,
            Long>, EmployeeContactDetailDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * This contains the contact details of the employee
 */
data class EmployeeContactDetailDTO(
    val id: Long? = null,
    /**
     * Contact details of the employee&#039;s contact
     * Sample: 09123456789
     */
    val contactDetails: String? = "",
    /**
     * Type of employee&#039;s contact
     * Sample: Mobile
     */
    val contactType: String? = "",
    val employee: EmployeeEmployeeDTO? = null
) : PassDTO<EmployeeContactDetail, Long>() {
    override fun toEntity(): EmployeeContactDetail =
            super.toEntityInternal(EmployeeContactDetailSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<EmployeeContactDetail,
            PassDTO<EmployeeContactDetail, Long>, Long>, PassDTO<EmployeeContactDetail, Long>,
            Long>>)

    override fun readId(): Long? = this.id
}

@Component
class EmployeeContactDetailSerializer : PassDtoSerializer<EmployeeContactDetail,
        EmployeeContactDetailDTO, Long>() {
    override fun toDto(entity: EmployeeContactDetail): EmployeeContactDetailDTO = cycle(entity) {
        EmployeeContactDetailDTO(
                id = entity.id,
        contactDetails = entity.contactDetails,
        contactType = entity.contactType,
        employee = entity.employee?.idDto() ?: entity.employee?.toDto()
                )}

    override fun toEntity(dto: EmployeeContactDetailDTO): EmployeeContactDetail =
            EmployeeContactDetail(
            id = dto.id,
    contactDetails = dto.contactDetails ?: "",
    contactType = dto.contactType ?: "",
    employee = dto.employee?.toEntity()
            )
    override fun idDto(id: Long): EmployeeContactDetailDTO = EmployeeContactDetailDTO(
            id = id,
    contactDetails = null,
    contactType = null,

            )}

@Service("employee.EmployeeContactDetailValidator")
class EmployeeContactDetailValidator : PassModelValidation<EmployeeContactDetail> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeContactDetail>):
            ValidatorBuilder<EmployeeContactDetail> = validatorBuilder.apply {
        konstraintOnObject(EmployeeContactDetail::employee) {
            notNull()
        }
    }
}

@Service("employee.EmployeeContactDetailDTOValidator")
class EmployeeContactDetailDTOValidator : PassModelValidation<EmployeeContactDetailDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeContactDetailDTO>):
            ValidatorBuilder<EmployeeContactDetailDTO> = validatorBuilder.apply {
        konstraintOnObject(EmployeeContactDetailDTO::employee) {
            notNull()
        }
    }
}
