package ffufm.patrick.api.spec.dbo.timerecord

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployee
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployeeDTO
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployeeSerializer
import ffufm.patrick.api.spec.dbo.project.ProjectProject
import ffufm.patrick.api.spec.dbo.project.ProjectProjectDTO
import ffufm.patrick.api.spec.dbo.project.ProjectProjectSerializer
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordTypeSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Boolean
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * This contains the time record model
 */
@Entity(name = "TimerecordTimeRecord")
@Table(name = "timerecord_timerecord")
data class TimerecordTimeRecord(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Date of the time record
     * Sample: 2022-02-01
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "date"
    )
    @Lob
    val date: String = "",
    /**
     * Category of the time record
     * Sample: Category 1
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "category"
    )
    @Lob
    val category: String = "",
    /**
     * Time in of the time record
     * Sample: 09:00:00
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "time_in"
    )
    @Lob
    val timeIn: String = "",
    /**
     * Time out of the time record
     * Sample: 18:00:00
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "time_out"
    )
    @Lob
    val timeOut: String = "",
    /**
     * Update status of the time record
     * Sample: FALSE
     */
    @Column(name = "is_updated")
    val isUpdated: Boolean = false,
    /**
     * Remarks/comment of the time record
     * Sample: First day record
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "remarks"
    )
    @Lob
    val remarks: String = "",
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val employee: EmployeeEmployee? = null,
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val timerecord: TimerecordTimeRecordType? = null,
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val projectTimerecord: ProjectProject? = null
) : PassDTOModel<TimerecordTimeRecord, TimerecordTimeRecordDTO, Long>() {
    override fun toDto(): TimerecordTimeRecordDTO =
            super.toDtoInternal(TimerecordTimeRecordSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TimerecordTimeRecord, TimerecordTimeRecordDTO,
            Long>, TimerecordTimeRecordDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * This contains the time record model
 */
data class TimerecordTimeRecordDTO(
    val id: Long? = null,
    /**
     * Date of the time record
     * Sample: 2022-02-01
     */
    val date: String? = "",
    /**
     * Category of the time record
     * Sample: Category 1
     */
    val category: String? = "",
    /**
     * Time in of the time record
     * Sample: 09:00:00
     */
    val timeIn: String? = "",
    /**
     * Time out of the time record
     * Sample: 18:00:00
     */
    val timeOut: String? = "",
    /**
     * Update status of the time record
     * Sample: FALSE
     */
    val isUpdated: Boolean? = false,
    /**
     * Remarks/comment of the time record
     * Sample: First day record
     */
    val remarks: String? = "",
    val employee: EmployeeEmployeeDTO? = null,
    val timerecord: TimerecordTimeRecordTypeDTO? = null,
    val projectTimerecord: ProjectProjectDTO? = null
) : PassDTO<TimerecordTimeRecord, Long>() {
    override fun toEntity(): TimerecordTimeRecord =
            super.toEntityInternal(TimerecordTimeRecordSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TimerecordTimeRecord,
            PassDTO<TimerecordTimeRecord, Long>, Long>, PassDTO<TimerecordTimeRecord, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class TimerecordTimeRecordSerializer : PassDtoSerializer<TimerecordTimeRecord,
        TimerecordTimeRecordDTO, Long>() {
    override fun toDto(entity: TimerecordTimeRecord): TimerecordTimeRecordDTO = cycle(entity) {
        TimerecordTimeRecordDTO(
                id = entity.id,
        date = entity.date,
        category = entity.category,
        timeIn = entity.timeIn,
        timeOut = entity.timeOut,
        isUpdated = entity.isUpdated,
        remarks = entity.remarks,
        employee = entity.employee?.idDto() ?: entity.employee?.toDto(),
        timerecord = entity.timerecord?.idDto() ?:
                entity.timerecord?.toDto(),
        projectTimerecord = entity.projectTimerecord?.idDto() ?: entity.projectTimerecord?.toDto()
                )}

    override fun toEntity(dto: TimerecordTimeRecordDTO): TimerecordTimeRecord =
            TimerecordTimeRecord(
            id = dto.id,
    date = dto.date ?: "",
    category = dto.category ?: "",
    timeIn = dto.timeIn ?: "",
    timeOut = dto.timeOut ?: "",
    isUpdated = dto.isUpdated ?: false,
    remarks = dto.remarks ?: "",
    employee = dto.employee?.toEntity(),
    timerecord = dto.timerecord?.toEntity(),
    projectTimerecord = dto.projectTimerecord?.toEntity()
            )
    override fun idDto(id: Long): TimerecordTimeRecordDTO = TimerecordTimeRecordDTO(
            id = id,
    date = null,
    category = null,
    timeIn = null,
    timeOut = null,
    isUpdated = null,
    remarks = null,

            )}

@Service("timerecord.TimerecordTimeRecordValidator")
class TimerecordTimeRecordValidator : PassModelValidation<TimerecordTimeRecord> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TimerecordTimeRecord>):
            ValidatorBuilder<TimerecordTimeRecord> = validatorBuilder.apply {
        konstraintOnObject(TimerecordTimeRecord::employee) {
            notNull()
        }
        konstraintOnObject(TimerecordTimeRecord::timerecord) {
            notNull()
        }
        konstraintOnObject(TimerecordTimeRecord::projectTimerecord) {
            notNull()
        }
    }
}

@Service("timerecord.TimerecordTimeRecordDTOValidator")
class TimerecordTimeRecordDTOValidator : PassModelValidation<TimerecordTimeRecordDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TimerecordTimeRecordDTO>):
            ValidatorBuilder<TimerecordTimeRecordDTO> = validatorBuilder.apply {
        konstraintOnObject(TimerecordTimeRecordDTO::employee) {
            notNull()
        }
        konstraintOnObject(TimerecordTimeRecordDTO::timerecord) {
            notNull()
        }
        konstraintOnObject(TimerecordTimeRecordDTO::projectTimerecord) {
            notNull()
        }
    }
}
