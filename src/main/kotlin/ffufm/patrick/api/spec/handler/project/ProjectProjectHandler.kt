package ffufm.patrick.api.spec.handler.project

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.patrick.api.spec.dbo.project.ProjectProjectDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface ProjectProjectDatabaseHandler {
    /**
     * Create Project: Creates a new Project object
     * HTTP Code 201: The created Project
     */
    suspend fun create(body: ProjectProjectDTO): ProjectProjectDTO

    /**
     * Find by Employee: Finds Projects by the parent Employee id
     * HTTP Code 200: List of Projects items
     */
    suspend fun getByEmployee(
        id: Long,
        maxResults: Int = 100,
        page: Int = 0
    ): Page<ProjectProjectDTO>

    /**
     * Finds Projects by ID: Returns Projects based on ID
     * HTTP Code 200: The Project object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): ProjectProjectDTO?

    /**
     * Delete Project by id.: Deletes one specific Project.
     */
    suspend fun remove(id: Long)

    /**
     * Update the Project: Updates an existing Project
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO
}

@Controller("project.Project")
class ProjectProjectHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: ProjectProjectDatabaseHandler

    /**
     * Create Project: Creates a new Project object
     * HTTP Code 201: The created Project
     */
    @RequestMapping(value = ["/projects/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: ProjectProjectDTO): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }

    /**
     * Find by Employee: Finds Projects by the parent Employee id
     * HTTP Code 200: List of Projects items
     */
    @RequestMapping(value = ["/employee/{id:\\d+}/projects/"], method = [RequestMethod.GET])
    suspend fun getByEmployee(
        @PathVariable("id") id: Long,
        @RequestParam("maxResults") maxResults: Int? = 100,
        @RequestParam("page") page: Int? = 0
    ): ResponseEntity<*> {

        return paging { databaseHandler.getByEmployee(id, maxResults ?: 100, page ?: 0) }
    }

    /**
     * Finds Projects by ID: Returns Projects based on ID
     * HTTP Code 200: The Project object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/projects/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Delete Project by id.: Deletes one specific Project.
     */
    @RequestMapping(value = ["/projects/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the Project: Updates an existing Project
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/projects/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: ProjectProjectDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
