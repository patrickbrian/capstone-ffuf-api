package ffufm.patrick.api.spec.handler.leaverecord

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveTypeDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface LeaverecordLeaveTypeDatabaseHandler {
    /**
     * Create LeaveType: Creates a new LeaveType object
     * HTTP Code 201: The created LeaveType
     */
    suspend fun create(body: LeaverecordLeaveTypeDTO): LeaverecordLeaveTypeDTO

    /**
     * Finds LeaveTypes by ID: Returns LeaveTypes based on ID
     * HTTP Code 200: The LeaveType object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): LeaverecordLeaveTypeDTO?

    /**
     * Delete LeaveType by id.: Deletes one specific LeaveType.
     */
    suspend fun remove(id: Long)

    /**
     * Update the LeaveType: Updates an existing LeaveType
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: LeaverecordLeaveTypeDTO, id: Long): LeaverecordLeaveTypeDTO
}

@Controller("leaverecord.LeaveType")
class LeaverecordLeaveTypeHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: LeaverecordLeaveTypeDatabaseHandler

    /**
     * Create LeaveType: Creates a new LeaveType object
     * HTTP Code 201: The created LeaveType
     */
    @RequestMapping(value = ["/leavetypes/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: LeaverecordLeaveTypeDTO): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }

    /**
     * Finds LeaveTypes by ID: Returns LeaveTypes based on ID
     * HTTP Code 200: The LeaveType object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/leavetypes/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Delete LeaveType by id.: Deletes one specific LeaveType.
     */
    @RequestMapping(value = ["/leavetypes/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the LeaveType: Updates an existing LeaveType
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/leavetypes/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: LeaverecordLeaveTypeDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
