package ffufm.patrick.api.spec.handler.timerecord

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface TimerecordTimeRecordDatabaseHandler {
    /**
     * Create TimeRecord: Creates a new TimeRecord object
     * HTTP Code 201: The created TimeRecord
     */
    suspend fun create(body: TimerecordTimeRecordDTO): TimerecordTimeRecordDTO

    /**
     * Get all TimeRecords: Returns all TimeRecords from the system that the user has access to.
     * HTTP Code 200: List of TimeRecords
     */
    suspend fun getAll(maxResults: Int = 100, page: Int = 0): Page<TimerecordTimeRecordDTO>

    /**
     * Find by Employee: Finds TimeRecords by the parent Employee id
     * HTTP Code 200: List of TimeRecords items
     */
    suspend fun getByEmployee(
        id: Long,
        maxResults: Int = 100,
        page: Int = 0
    ): Page<TimerecordTimeRecordDTO>

    /**
     * Finds TimeRecords by ID: Returns TimeRecords based on ID
     * HTTP Code 200: The TimeRecord object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): TimerecordTimeRecordDTO?

    /**
     * Delete TimeRecord by id.: Deletes one specific TimeRecord.
     */
    suspend fun remove(id: Long)

    /**
     * Update the TimeRecord: Updates an existing TimeRecord
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: TimerecordTimeRecordDTO, id: Long): TimerecordTimeRecordDTO
}

@Controller("timerecord.TimeRecord")
class TimerecordTimeRecordHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: TimerecordTimeRecordDatabaseHandler

    /**
     * Create TimeRecord: Creates a new TimeRecord object
     * HTTP Code 201: The created TimeRecord
     */
    @RequestMapping(value = ["/timerecords/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: TimerecordTimeRecordDTO): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }

    /**
     * Get all TimeRecords: Returns all TimeRecords from the system that the user has access to.
     * HTTP Code 200: List of TimeRecords
     */
    @RequestMapping(value = ["/timerecords/"], method = [RequestMethod.GET])
    suspend fun getAll(@RequestParam("maxResults") maxResults: Int? = 100, @RequestParam("page")
            page: Int? = 0): ResponseEntity<*> {

        return paging { databaseHandler.getAll(maxResults ?: 100, page ?: 0) }
    }

    /**
     * Find by Employee: Finds TimeRecords by the parent Employee id
     * HTTP Code 200: List of TimeRecords items
     */
    @RequestMapping(value = ["/employee/{id:\\d+}/timerecords/"], method = [RequestMethod.GET])
    suspend fun getByEmployee(
        @PathVariable("id") id: Long,
        @RequestParam("maxResults") maxResults: Int? = 100,
        @RequestParam("page") page: Int? = 0
    ): ResponseEntity<*> {

        return paging { databaseHandler.getByEmployee(id, maxResults ?: 100, page ?: 0) }
    }

    /**
     * Finds TimeRecords by ID: Returns TimeRecords based on ID
     * HTTP Code 200: The TimeRecord object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/timerecords/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Delete TimeRecord by id.: Deletes one specific TimeRecord.
     */
    @RequestMapping(value = ["/timerecords/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the TimeRecord: Updates an existing TimeRecord
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/timerecords/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: TimerecordTimeRecordDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
