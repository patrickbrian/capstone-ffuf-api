package ffufm.patrick.api.spec.handler.timerecord

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordTypeDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface TimerecordTimeRecordTypeDatabaseHandler {
    /**
     * Create TimeRecordType: Creates a new TimeRecordType object
     * HTTP Code 201: The created TimeRecordType
     */
    suspend fun create(body: TimerecordTimeRecordTypeDTO): TimerecordTimeRecordTypeDTO

    /**
     * Finds TimeRecordTypes by ID: Returns TimeRecordTypes based on ID
     * HTTP Code 200: The TimeRecordType object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): TimerecordTimeRecordTypeDTO?

    /**
     * Delete TimeRecordType by id.: Deletes one specific TimeRecordType.
     */
    suspend fun remove(id: Long)

    /**
     * Update the TimeRecordType: Updates an existing TimeRecordType
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: TimerecordTimeRecordTypeDTO, id: Long): TimerecordTimeRecordTypeDTO
}

@Controller("timerecord.TimeRecordType")
class TimerecordTimeRecordTypeHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: TimerecordTimeRecordTypeDatabaseHandler

    /**
     * Create TimeRecordType: Creates a new TimeRecordType object
     * HTTP Code 201: The created TimeRecordType
     */
    @RequestMapping(value = ["/timerecordtypes/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: TimerecordTimeRecordTypeDTO): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }

    /**
     * Finds TimeRecordTypes by ID: Returns TimeRecordTypes based on ID
     * HTTP Code 200: The TimeRecordType object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/timerecordtypes/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Delete TimeRecordType by id.: Deletes one specific TimeRecordType.
     */
    @RequestMapping(value = ["/timerecordtypes/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the TimeRecordType: Updates an existing TimeRecordType
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/timerecordtypes/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: TimerecordTimeRecordTypeDTO, @PathVariable("id")
            id: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
