package ffufm.patrick.api.spec.handler.employee

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.patrick.api.spec.dbo.employee.EmployeeAddressDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface EmployeeAddressDatabaseHandler {
    /**
     * Create Address: Creates a new Address object
     * HTTP Code 201: The created Address
     */
    suspend fun create(body: EmployeeAddressDTO): EmployeeAddressDTO

    /**
     * Finds Addresses by ID: Returns Addresses based on ID
     * HTTP Code 200: The Address object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): EmployeeAddressDTO?

    /**
     * Delete Address by id.: Deletes one specific Address.
     */
    suspend fun remove(id: Long)

    /**
     * Update the Address: Updates an existing Address
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: EmployeeAddressDTO, id: Long): EmployeeAddressDTO
}

@Controller("employee.Address")
class EmployeeAddressHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: EmployeeAddressDatabaseHandler

    /**
     * Create Address: Creates a new Address object
     * HTTP Code 201: The created Address
     */
    @RequestMapping(value = ["/addresses/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: EmployeeAddressDTO): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }

    /**
     * Finds Addresses by ID: Returns Addresses based on ID
     * HTTP Code 200: The Address object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/addresses/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Delete Address by id.: Deletes one specific Address.
     */
    @RequestMapping(value = ["/addresses/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the Address: Updates an existing Address
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/addresses/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: EmployeeAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
