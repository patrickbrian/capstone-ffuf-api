package ffufm.patrick.api.spec.handler.employee

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployeeDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface EmployeeEmployeeDatabaseHandler {
    /**
     * Create Employee: Creates a new Employee object
     * HTTP Code 201: The created Employee
     */
    suspend fun create(body: EmployeeEmployeeDTO): EmployeeEmployeeDTO

    /**
     * Get all Employees: Returns all Employees from the system that the user has access to.
     * HTTP Code 200: List of Employees
     */
    suspend fun getAll(maxResults: Int = 100, page: Int = 0): Page<EmployeeEmployeeDTO>

    /**
     * Finds Employees by ID: Returns Employees based on ID
     * HTTP Code 200: The Employee object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): EmployeeEmployeeDTO?

    /**
     * Delete Employee by id.: Deletes one specific Employee.
     */
    suspend fun remove(id: Long)

    /**
     * Update the Employee: Updates an existing Employee
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: EmployeeEmployeeDTO, id: Long): EmployeeEmployeeDTO
}

@Controller("employee.Employee")
class EmployeeEmployeeHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: EmployeeEmployeeDatabaseHandler

    /**
     * Create Employee: Creates a new Employee object
     * HTTP Code 201: The created Employee
     */
    @RequestMapping(value = ["/employee/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: EmployeeEmployeeDTO): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }

    /**
     * Get all Employees: Returns all Employees from the system that the user has access to.
     * HTTP Code 200: List of Employees
     */
    @RequestMapping(value = ["/employee/"], method = [RequestMethod.GET])
    suspend fun getAll(@RequestParam("maxResults") maxResults: Int? = 100, @RequestParam("page")
            page: Int? = 0): ResponseEntity<*> {

        return paging { databaseHandler.getAll(maxResults ?: 100, page ?: 0) }
    }

    /**
     * Finds Employees by ID: Returns Employees based on ID
     * HTTP Code 200: The Employee object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/employee/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Delete Employee by id.: Deletes one specific Employee.
     */
    @RequestMapping(value = ["/employee/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the Employee: Updates an existing Employee
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/employee/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: EmployeeEmployeeDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
