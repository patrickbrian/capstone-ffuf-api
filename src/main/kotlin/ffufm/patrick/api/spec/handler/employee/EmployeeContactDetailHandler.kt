package ffufm.patrick.api.spec.handler.employee

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.patrick.api.spec.dbo.employee.EmployeeContactDetailDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface EmployeeContactDetailDatabaseHandler {
    /**
     * Create ContactDetail: Creates a new ContactDetail object
     * HTTP Code 201: The created ContactDetail
     */
    suspend fun create(body: EmployeeContactDetailDTO): EmployeeContactDetailDTO

    /**
     * Finds ContactDetails by ID: Returns ContactDetails based on ID
     * HTTP Code 200: The ContactDetail object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): EmployeeContactDetailDTO?

    /**
     * Delete ContactDetail by id.: Deletes one specific ContactDetail.
     */
    suspend fun remove(id: Long)

    /**
     * Update the ContactDetail: Updates an existing ContactDetail
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: EmployeeContactDetailDTO, id: Long): EmployeeContactDetailDTO
}

@Controller("employee.ContactDetail")
class EmployeeContactDetailHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: EmployeeContactDetailDatabaseHandler

    /**
     * Create ContactDetail: Creates a new ContactDetail object
     * HTTP Code 201: The created ContactDetail
     */
    @RequestMapping(value = ["/contactdetails/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: EmployeeContactDetailDTO): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }

    /**
     * Finds ContactDetails by ID: Returns ContactDetails based on ID
     * HTTP Code 200: The ContactDetail object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/contactdetails/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Delete ContactDetail by id.: Deletes one specific ContactDetail.
     */
    @RequestMapping(value = ["/contactdetails/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the ContactDetail: Updates an existing ContactDetail
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/contactdetails/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: EmployeeContactDetailDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
