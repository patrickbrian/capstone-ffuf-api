package ffufm.patrick.api.handlerimpl.timerecord

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.patrick.api.repositories.timerecord.TimerecordTimeRecordRepository
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecord
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordDTO
import ffufm.patrick.api.spec.handler.timerecord.TimerecordTimeRecordDatabaseHandler
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("timerecord.TimerecordTimeRecordHandler")
class TimerecordTimeRecordHandlerImpl : PassDatabaseHandler<TimerecordTimeRecord,
        TimerecordTimeRecordRepository>(), TimerecordTimeRecordDatabaseHandler {
    /**
     * Create TimeRecord: Creates a new TimeRecord object
     * HTTP Code 201: The created TimeRecord
     */
    override suspend fun create(body: TimerecordTimeRecordDTO): TimerecordTimeRecordDTO {
        val bodyEntity = body.toEntity()
        return repository.save(bodyEntity).toDto()
    }

    /**
     * Get all TimeRecords: Returns all TimeRecords from the system that the user has access to.
     * HTTP Code 200: List of TimeRecords
     */
    override suspend fun getAll(maxResults: Int, page: Int): Page<TimerecordTimeRecordDTO> {

        return repository.findAll(Pageable.unpaged()).toDtos()
    }

    /**
     * Find by Employee: Finds TimeRecords by the parent Employee id
     * HTTP Code 200: List of TimeRecords items
     */
    override suspend fun getByEmployee(
        id: Long,
        maxResults: Int,
        page: Int
    ): Page<TimerecordTimeRecordDTO> {
        return repository.findAll(Pageable.unpaged()).toDtos()
    }

    /**
     * Finds TimeRecords by ID: Returns TimeRecords based on ID
     * HTTP Code 200: The TimeRecord object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): TimerecordTimeRecordDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Delete TimeRecord by id.: Deletes one specific TimeRecord.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the TimeRecord: Updates an existing TimeRecord
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: TimerecordTimeRecordDTO, id: Long): TimerecordTimeRecordDTO {
        val original = repository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()
        return repository.save(original.copy(
            date = bodyEntity.date,
            category = bodyEntity.category,
            timeIn = bodyEntity.timeIn,
            timeOut = bodyEntity.timeOut,
            isUpdated = bodyEntity.isUpdated,
            remarks = bodyEntity.remarks
        )).toDto()
    }
}
