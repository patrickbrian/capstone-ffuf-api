package ffufm.patrick.api.handlerimpl.timerecord

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.patrick.api.repositories.timerecord.TimerecordTimeRecordTypeRepository
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordType
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordTypeDTO
import ffufm.patrick.api.spec.handler.timerecord.TimerecordTimeRecordTypeDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("timerecord.TimerecordTimeRecordTypeHandler")
class TimerecordTimeRecordTypeHandlerImpl : PassDatabaseHandler<TimerecordTimeRecordType,
        TimerecordTimeRecordTypeRepository>(), TimerecordTimeRecordTypeDatabaseHandler {
    /**
     * Create TimeRecordType: Creates a new TimeRecordType object
     * HTTP Code 201: The created TimeRecordType
     */
    override suspend fun create(body: TimerecordTimeRecordTypeDTO): TimerecordTimeRecordTypeDTO {
        val bodyEntity = body.toEntity()
        return repository.save(bodyEntity).toDto()
    }

    /**
     * Finds TimeRecordTypes by ID: Returns TimeRecordTypes based on ID
     * HTTP Code 200: The TimeRecordType object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): TimerecordTimeRecordTypeDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Delete TimeRecordType by id.: Deletes one specific TimeRecordType.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the TimeRecordType: Updates an existing TimeRecordType
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: TimerecordTimeRecordTypeDTO, id: Long):
            TimerecordTimeRecordTypeDTO {
        val original = repository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()
        return repository.save(original.copy(
            shortName = bodyEntity.shortName,
            longName = bodyEntity.longName,
            numberOfHours = bodyEntity.numberOfHours
        )).toDto()
    }
}
