package ffufm.patrick.api.handlerimpl.leaverecord

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.patrick.api.repositories.leaverecord.LeaverecordLeaveRecordRepository
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveRecordDTO
import ffufm.patrick.api.spec.handler.leaverecord.LeaverecordLeaveRecordDatabaseHandler
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("leaverecord.LeaverecordLeaveRecordHandler")
class LeaverecordLeaveRecordHandlerImpl : PassDatabaseHandler<LeaverecordLeaveRecord,
        LeaverecordLeaveRecordRepository>(), LeaverecordLeaveRecordDatabaseHandler {
    /**
     * Create LeaveRecord: Creates a new LeaveRecord object
     * HTTP Code 201: The created LeaveRecord
     */
    override suspend fun create(body: LeaverecordLeaveRecordDTO): LeaverecordLeaveRecordDTO {
        val bodyEntity = body.toEntity()
        return repository.save(bodyEntity).toDto()
    }

    /**
     * Get all LeaveRecords: Returns all LeaveRecords from the system that the user has access to.
     * HTTP Code 200: List of LeaveRecords
     */
    override suspend fun getAll(maxResults: Int, page: Int):
            Page<LeaverecordLeaveRecordDTO> {

        return repository.findAll(Pageable.unpaged()).toDtos()
    }

    /**
     * Find by Employee: Finds LeaveRecords by the parent Employee id
     * HTTP Code 200: List of LeaveRecords items
     */
    override suspend fun getByEmployee(
        id: Long,
        maxResults: Int,
        page: Int
    ): Page<LeaverecordLeaveRecordDTO> {
        return repository.findAll(Pageable.unpaged()).toDtos()
    }

    /**
     * Finds LeaveRecords by ID: Returns LeaveRecords based on ID
     * HTTP Code 200: The LeaveRecord object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): LeaverecordLeaveRecordDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Delete LeaveRecord by id.: Deletes one specific LeaveRecord.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the LeaveRecord: Updates an existing LeaveRecord
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: LeaverecordLeaveRecordDTO, id: Long): LeaverecordLeaveRecordDTO {
        val original = repository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()

        return repository.save(original.copy(
            date = bodyEntity.date,
        )).toDto()
    }
}
