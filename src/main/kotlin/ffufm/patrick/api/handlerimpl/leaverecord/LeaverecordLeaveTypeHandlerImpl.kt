package ffufm.patrick.api.handlerimpl.leaverecord

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.patrick.api.repositories.leaverecord.LeaverecordLeaveTypeRepository
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveType
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveTypeDTO
import ffufm.patrick.api.spec.handler.leaverecord.LeaverecordLeaveTypeDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("leaverecord.LeaverecordLeaveTypeHandler")
class LeaverecordLeaveTypeHandlerImpl : PassDatabaseHandler<LeaverecordLeaveType,
        LeaverecordLeaveTypeRepository>(), LeaverecordLeaveTypeDatabaseHandler {
    /**
     * Create LeaveType: Creates a new LeaveType object
     * HTTP Code 201: The created LeaveType
     */
    override suspend fun create(body: LeaverecordLeaveTypeDTO): LeaverecordLeaveTypeDTO {
        val bodyEntity = body.toEntity()
        return repository.save(bodyEntity).toDto()
    }

    /**
     * Finds LeaveTypes by ID: Returns LeaveTypes based on ID
     * HTTP Code 200: The LeaveType object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): LeaverecordLeaveTypeDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Delete LeaveType by id.: Deletes one specific LeaveType.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the LeaveType: Updates an existing LeaveType
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: LeaverecordLeaveTypeDTO, id: Long): LeaverecordLeaveTypeDTO {
        val original = repository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()
        return repository.save(original.copy(
            shortName = bodyEntity.shortName,
            maximumAllowed = bodyEntity.maximumAllowed,
            longName = bodyEntity.longName
        )).toDto()
    }
}
