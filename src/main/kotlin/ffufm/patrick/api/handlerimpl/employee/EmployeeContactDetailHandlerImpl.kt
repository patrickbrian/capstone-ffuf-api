package ffufm.patrick.api.handlerimpl.employee

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.patrick.api.repositories.employee.EmployeeContactDetailRepository
import ffufm.patrick.api.spec.dbo.employee.EmployeeContactDetail
import ffufm.patrick.api.spec.dbo.employee.EmployeeContactDetailDTO
import ffufm.patrick.api.spec.handler.employee.EmployeeContactDetailDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("employee.EmployeeContactDetailHandler")
class EmployeeContactDetailHandlerImpl : PassDatabaseHandler<EmployeeContactDetail,
        EmployeeContactDetailRepository>(), EmployeeContactDetailDatabaseHandler {
    /**
     * Create ContactDetail: Creates a new ContactDetail object
     * HTTP Code 201: The created ContactDetail
     */
    override suspend fun create(body: EmployeeContactDetailDTO): EmployeeContactDetailDTO {
        val bodyEntity = body.toEntity()
        return repository.save(bodyEntity).toDto()
    }

    /**
     * Finds ContactDetails by ID: Returns ContactDetails based on ID
     * HTTP Code 200: The ContactDetail object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): EmployeeContactDetailDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Delete ContactDetail by id.: Deletes one specific ContactDetail.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the ContactDetail: Updates an existing ContactDetail
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: EmployeeContactDetailDTO, id: Long): EmployeeContactDetailDTO {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.save(original).toDto()
    }
}
