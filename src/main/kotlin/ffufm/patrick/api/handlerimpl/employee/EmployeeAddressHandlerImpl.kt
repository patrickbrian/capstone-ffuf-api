package ffufm.patrick.api.handlerimpl.employee

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.patrick.api.repositories.employee.EmployeeAddressRepository
import ffufm.patrick.api.spec.dbo.employee.EmployeeAddress
import ffufm.patrick.api.spec.dbo.employee.EmployeeAddressDTO
import ffufm.patrick.api.spec.handler.employee.EmployeeAddressDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("employee.EmployeeAddressHandler")
class EmployeeAddressHandlerImpl : PassDatabaseHandler<EmployeeAddress,
        EmployeeAddressRepository>(), EmployeeAddressDatabaseHandler {
    /**
     * Create Address: Creates a new Address object
     * HTTP Code 201: The created Address
     */
    override suspend fun create(body: EmployeeAddressDTO): EmployeeAddressDTO {
        val bodyEntity = body.toEntity()
        return repository.save(bodyEntity).toDto()
    }

    /**
     * Finds Addresses by ID: Returns Addresses based on ID
     * HTTP Code 200: The Address object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): EmployeeAddressDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Delete Address by id.: Deletes one specific Address.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the Address: Updates an existing Address
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: EmployeeAddressDTO, id: Long): EmployeeAddressDTO {
        val original = repository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()

        return repository.save(original.copy(
            street = bodyEntity.street,
            barangay = bodyEntity.barangay,
            city = bodyEntity.city,
            province = bodyEntity.province,
            zipCode = bodyEntity.zipCode
        )).toDto()
    }
}
