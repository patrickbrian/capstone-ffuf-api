package ffufm.patrick.api.handlerimpl.employee

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.patrick.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployee
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployeeDTO
import ffufm.patrick.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("employee.EmployeeEmployeeHandler")
class EmployeeEmployeeHandlerImpl : PassDatabaseHandler<EmployeeEmployee,
        EmployeeEmployeeRepository>(), EmployeeEmployeeDatabaseHandler {
    /**
     * Create Employee: Creates a new Employee object
     * HTTP Code 201: The created Employee
     */
    override suspend fun create(body: EmployeeEmployeeDTO): EmployeeEmployeeDTO {
        val bodyEntity = body.toEntity()
        return repository.save(bodyEntity).toDto()
    }

    /**
     * Get all Employees: Returns all Employees from the system that the user has access to.
     * HTTP Code 200: List of Employees
     */
    override suspend fun getAll(maxResults: Int, page: Int): Page<EmployeeEmployeeDTO> {
        return repository.findAll(Pageable.unpaged()).toDtos()
    }

    /**
     * Finds Employees by ID: Returns Employees based on ID
     * HTTP Code 200: The Employee object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): EmployeeEmployeeDTO? {

        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Delete Employee by id.: Deletes one specific Employee.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.delete(original)
    }

    /**
     * Update the Employee: Updates an existing Employee
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: EmployeeEmployeeDTO, id: Long): EmployeeEmployeeDTO {
        val original = repository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()

        return repository.save(original.copy(
            firstName = bodyEntity.firstName,
            lastName = bodyEntity.lastName,
            email = bodyEntity.email,
            employeeType = bodyEntity.employeeType
        )).toDto()
    }
}
