package ffufm.patrick.api.handlerimpl.project

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.patrick.api.repositories.project.ProjectProjectRepository
import ffufm.patrick.api.spec.dbo.project.ProjectProject
import ffufm.patrick.api.spec.dbo.project.ProjectProjectDTO
import ffufm.patrick.api.spec.handler.project.ProjectProjectDatabaseHandler
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("project.ProjectProjectHandler")
class ProjectProjectHandlerImpl : PassDatabaseHandler<ProjectProject, ProjectProjectRepository>(),
        ProjectProjectDatabaseHandler {
    /**
     * Create Project: Creates a new Project object
     * HTTP Code 201: The created Project
     */
    override suspend fun create(body: ProjectProjectDTO): ProjectProjectDTO {
        val bodyEntity = body.toEntity()
        return repository.save(bodyEntity).toDto()
    }

    /**
     * Find by Employee: Finds Projects by the parent Employee id
     * HTTP Code 200: List of Projects items
     */
    override suspend fun getByEmployee(
        id: Long,
        maxResults: Int,
        page: Int
    ): Page<ProjectProjectDTO> {
        return repository.findAll(Pageable.unpaged()).toDtos()
    }

    /**
     * Finds Projects by ID: Returns Projects based on ID
     * HTTP Code 200: The Project object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): ProjectProjectDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Delete Project by id.: Deletes one specific Project.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the Project: Updates an existing Project
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {
        val original = repository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()
        return repository.save(original.copy(
            name = bodyEntity.name,
            description = bodyEntity.description
        )).toDto()
    }
}
