package ffufm.patrick.api.repositories.project

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.patrick.api.spec.dbo.project.ProjectProject
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ProjectProjectRepository : PassRepository<ProjectProject, Long> {
    @Query(
        "SELECT t from ProjectProject t LEFT JOIN FETCH t.employee",
        countQuery = "SELECT count(id) FROM ProjectProject"
    )
    fun findAllAndFetchEmployee(pageable: Pageable): Page<ProjectProject>
}
