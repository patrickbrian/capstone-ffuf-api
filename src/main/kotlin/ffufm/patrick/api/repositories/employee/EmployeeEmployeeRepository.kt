package ffufm.patrick.api.repositories.employee

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.patrick.api.spec.dbo.employee.EmployeeEmployee
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface EmployeeEmployeeRepository : PassRepository<EmployeeEmployee, Long> {
    @Query(
        "SELECT t from EmployeeEmployee t LEFT JOIN FETCH t.addresses",
        countQuery = "SELECT count(id) FROM EmployeeEmployee"
    )
    fun findAllAndFetchAddresses(pageable: Pageable): Page<EmployeeEmployee>

    @Query(
        "SELECT t from EmployeeEmployee t LEFT JOIN FETCH t.contactDetails",
        countQuery = "SELECT count(id) FROM EmployeeEmployee"
    )
    fun findAllAndFetchContactDetails(pageable: Pageable): Page<EmployeeEmployee>

    @Query(
        "SELECT t from EmployeeEmployee t LEFT JOIN FETCH t.timeRecords",
        countQuery = "SELECT count(id) FROM EmployeeEmployee"
    )
    fun findAllAndFetchTimeRecords(pageable: Pageable): Page<EmployeeEmployee>

    @Query(
        "SELECT t from EmployeeEmployee t LEFT JOIN FETCH t.projects",
        countQuery = "SELECT count(id) FROM EmployeeEmployee"
    )
    fun findAllAndFetchProjects(pageable: Pageable): Page<EmployeeEmployee>

    @Query(
        "SELECT t from EmployeeEmployee t LEFT JOIN FETCH t.leaveRecords",
        countQuery = "SELECT count(id) FROM EmployeeEmployee"
    )
    fun findAllAndFetchLeaveRecords(pageable: Pageable): Page<EmployeeEmployee>
}
