package ffufm.patrick.api.repositories.employee

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.patrick.api.spec.dbo.employee.EmployeeContactDetail
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface EmployeeContactDetailRepository : PassRepository<EmployeeContactDetail, Long> {
    @Query(
        "SELECT t from EmployeeContactDetail t LEFT JOIN FETCH t.employee",
        countQuery = "SELECT count(id) FROM EmployeeContactDetail"
    )
    fun findAllAndFetchEmployee(pageable: Pageable): Page<EmployeeContactDetail>
}
