package ffufm.patrick.api.repositories.timerecord

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecordType
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface TimerecordTimeRecordTypeRepository : PassRepository<TimerecordTimeRecordType, Long> {
    @Query(
        "SELECT t from TimerecordTimeRecordType t LEFT JOIN FETCH t.timeRecords",
        countQuery = "SELECT count(id) FROM TimerecordTimeRecordType"
    )
    fun findAllAndFetchTimeRecords(pageable: Pageable): Page<TimerecordTimeRecordType>
}
