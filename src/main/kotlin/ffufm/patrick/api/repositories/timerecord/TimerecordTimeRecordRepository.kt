package ffufm.patrick.api.repositories.timerecord

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.patrick.api.spec.dbo.timerecord.TimerecordTimeRecord
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface TimerecordTimeRecordRepository : PassRepository<TimerecordTimeRecord, Long> {
    @Query(
        "SELECT t from TimerecordTimeRecord t LEFT JOIN FETCH t.employee",
        countQuery = "SELECT count(id) FROM TimerecordTimeRecord"
    )
    fun findAllAndFetchEmployee(pageable: Pageable): Page<TimerecordTimeRecord>

    @Query(
        "SELECT t from TimerecordTimeRecord t LEFT JOIN FETCH t.timerecord",
        countQuery = "SELECT count(id) FROM TimerecordTimeRecord"
    )
    fun findAllAndFetchTimerecord(pageable: Pageable): Page<TimerecordTimeRecord>
}
