package ffufm.patrick.api.repositories.leaverecord

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveType
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface LeaverecordLeaveTypeRepository : PassRepository<LeaverecordLeaveType, Long> {
    @Query(
        "SELECT t from LeaverecordLeaveType t LEFT JOIN FETCH t.leaveRecords",
        countQuery = "SELECT count(id) FROM LeaverecordLeaveType"
    )
    fun findAllAndFetchLeaveRecords(pageable: Pageable): Page<LeaverecordLeaveType>
}
