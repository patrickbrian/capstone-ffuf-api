package ffufm.patrick.api.repositories.leaverecord

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.patrick.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface LeaverecordLeaveRecordRepository : PassRepository<LeaverecordLeaveRecord, Long> {
    @Query(
        "SELECT t from LeaverecordLeaveRecord t LEFT JOIN FETCH t.leaverecord",
        countQuery = "SELECT count(id) FROM LeaverecordLeaveRecord"
    )
    fun findAllAndFetchLeaverecord(pageable: Pageable): Page<LeaverecordLeaveRecord>

    @Query(
        "SELECT t from LeaverecordLeaveRecord t LEFT JOIN FETCH t.employee",
        countQuery = "SELECT count(id) FROM LeaverecordLeaveRecord"
    )
    fun findAllAndFetchEmployee(pageable: Pageable): Page<LeaverecordLeaveRecord>
}
