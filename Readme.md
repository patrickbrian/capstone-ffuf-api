# Capstone using FFUF API

## Endpoints
    1. Employee
        a. Create, Update, Delete, Get all and Get by id employee
        b. Create, Update, Delete, Get all and Get by id employee address
        c. Create, Update, Delete, Get all and Get by id employee contact detail
    2. Leave Record
        a. Create, Update, Delete, and  Get by id leave record type
        b. Create, Update, Delete, Get all, Get by employee, and  Get by id leave record
    3. Time Record
        a. Create, Update, Delete, and  Get by id time record type
        b. Create, Update, Delete, Get all, Get by employee, and  Get by id time record
    4. Project
        ga. Create, Update, Delete, Get by employee and  Get by id project